var Macros=require("./js/macros.js");
const pUtil=require("./js/pathUtils.js");

var global={};

module.exports = {
	file2macro: function(file) {
		return Macros.fileToMacro(file);
	},
	addSearchPath: function(dir) {
		var dir=pUtil.toAbsolute(dir);
		if(Macros.defaultContext.searchPaths.indexOf(dir)==-1) {
			Macros.defaultContext.searchPaths.push(dir);
		}
	},
	renderFile: function(macros, destination, fn) {
		Macros.renderFile(macros,destination,global,null, fn);
	},
	global: global
}
