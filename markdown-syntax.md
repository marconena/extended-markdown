# # Markdown syntax

Accepted markdown syntax is

## # Text formatting

For formatting the text existing directive:

* **Bold**: use a `**text bold**` result is:  **text bold**
* **Italic**: use a `//Italic text//` result is: //Italic text//
* **Strikethrough**: use a `--Strikethrough text--` result is: --Strikethrough text--
* **Underline**: use a `__Underline text__` result is: __Underline text__

**Example:**

```md
This is a test text whith **bold** and __underline__ on //italic// end --strikethrough--.

Text can join text format **//italic and bold//** or --//italic and strikethrough//-- or **__bold with underline__**
```

_Result:_

This is a test text whith **bold** and __underline__ on //italic// end --strikethrough--.

Text can join text format **//italic and bold//** or --//italic and strikethrough//-- or **__bold with underline__**


## # Titles

Titles is on format

**Example:**

```md

# -Indice
{{toc}}
# # Titolo 1
## # Titolo 1.1
## # Titolo 1.2
### # Titolo 1.2.1
# Note ( _Title not numerated_ )
## -Title not on table of context

```

First char  `#` can repeat and is a level of title, for example `#` for first level title and `##` for second level.

Character after a title level can are:
* `#` auto number title
* `-` to remove from table of context.

On markdown can add a macro command `{{toc}}` this command insert a table of context for document.


## # Quote

For quote can use a `>` at start of line and repeat for number of quote level.

**Example:**

```md
> Quote level 1
> this is a test of quote
>> And this is quote level 2
> Return to quote level 1
```

_Result:_

> Quote level 1
> Second line of quote
>> And this is quote level 2
> Return to quote level 1

## # Lists

For list must start a line with a `tab` of `space` chars and `*` or `-` or `[0-9].`:

Two `space` is equal to one `tab` and a tab increment an level of list.

* `*`: for circle prefix symbol
* `-`: for square prefix symbol
* `.#`: For auto number list

**Example:**

```md
* Level 1 (circle)
  * Level 1.1 (circle)
  - Level 1.2 (square)
  .# Level 1.3 (none)
2. Level 2 (number)
  - Level 2.1 (square)
  .# Level 2.2  (number)
```

_Result:_

* Level 1 (circle)
  * Level 1.1 (circle)
  - Level 1.2 (square)
  .# Level 1.3 (none)
2. Level 2 (number)
  - Level 2.1 (square)
  .# Level 2.2  (number)

## # Link and ids

For a link a syntax is: `[Text](link)`:

* `Text`: text of link
* `link`: link url can is `http://www.google.it` for external hyperlink or `myId` for a on document defined id.

For is a syntax is: `[Text]"Id"`:

* `Text`: text of id
* `Id`: identifier

**Example:**

```md
External link [Google Home](http://www.google.com)

On document identifier: [Go to may identifier](myId)


[My identifier]"myId"
```

_Result:_

External link [Google Home](http://www.google.com)

On document identifier: [Go to may identifier](myId)


[My identifier]"myId"

## # Images

Images can add with syntax: `![Description](src)`:

* `Description`: Image Description
* `scr`: Relative or absolute image url

**Example**:

```md
Remote image: ![Image of _Yaktocat_](https://octodex.github.com/images/yaktocat.png)

Local image: ![Immagine locale](tests/cell.png)
```

_Result:_

Remote image: ![Image of _Google_](https://www.google.it/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png)

Local image: ![Local image](test/cell.png)

## # Horizontal line and page break

Horizontal line is with a `---` or `===` at start of line:
* `---`: for single line
* `===`: for double line
For page break use `^^^` at start of line.

**Example:**

```md
Line single
---
Line double
===
Page break
^^^
This is on next page
```

_Result:_

Line single
---
Line double
===
Page break
^^^
This is on next page


## # Tables


**Example:**

```md
| GRP 1 |-| total |
| title1 | title2 | title3 |
|--------|:-------|-------:|
|: Left | Text 2 ABCDEFGHIJKLMNOPQRSTUVWXYZ | Text 3 ABCDEFGHIJKLMNOPQRSTUVWXYZ  |
| Right :| Cell expanded to next cell |-|
|: Center :| Text 2 | Text 3 |
|: text multilinee\
\
second _line_\
\
* list 1\
* list 2 | Text 2 | **Sub table**:\
\
\| title \|\
\|-------\|\
\| my **text** \|\
\| Row **2** \|\
\| Row **3** \|\
:|
| Rowspan row :|-| **Total** |

```

_Result:_

| GRP 1 |-| total |
| title1 | title2 | title3 |
|--------|:-------|-------:|
|: Left | Text 2 ABCDEFGHIJKLMNOPQRSTUVWXYZ | Text 3 ABCDEFGHIJKLMNOPQRSTUVWXYZ  |
| Right :| Cell expanded to next cell |-|
|: Center :| Text 2 | Text 3 |
|: text multilinee\
\
second _line_\
\
* list 1\
* list 2 | Text 2 | **Sub table**:\
\
\| title \|\
\|-------\|\
\| my **text** \|\
\| Row **2** \|\
\| Row **3** \|\
Fine tabella \
:|
| Rowspan row :|-| **Total** |

## # Code

For code section there is two different mode:

### # Code in line

Inside a text can put inline code with macro `\`< inlinecode >\``, for put char ` \`` must add befor a erasing char like this `\\``.


### # Multiline

For multiline code must start with row `\`\`\`< type >` where:

* `< type >`: is a source type of source (json,xml,javascript)

And end with row equals to `\`\`\``.

**Example:**

```md
Codice _XML_:

```xml
<html>
 <head>
 </head>
 <body onLoad="javascript:loaded()" >
  <h1>TITLE: for example</h1>
 </body>
</html>
 ```

Codice _JSON_:

```json
{
  "number": 100,
  "String": "Mia \"stringa\"",
  "boolean": true,
  "object": {
    "name": "Marco",
    "enabled": false
  }
}
 ```

```

_Result:_

Codice _XML_:

```xml
<html>
 <head>
 </head>
 <body onLoad="javascript:loaded()" >
  <h1>TITLE: for example</h1>
 </body>
</html>
```
Codice _JSON_:

```json
{
  "number": 100,
  "String": "Mia \"stringa\"",
  "boolean": true,
  "object": {
    "name": "Marco",
    "enabled": false
  }
}
```
