var mdLib=require("../macros");
const utl=require("../util.js")
const pUtil=require("../pathUtils.js");
const EventEmitter = require('events');
const vm = require('vm');

const ee = new EventEmitter();

function Render(searchPath) {
  // Path to search
  this.searchPaths=searchPath.slice(0);
  this.searchPaths.push(pUtil.toAbsolute(__dirname+"/../../templates"));
  mdLib.defaultContext.searchPaths=searchPath;
  
}

Render.prototype.type="undefined";
Render.prototype.template="undefined";
Render.prototype.globals={};

Render.prototype.findFile=function(file,context) {
  return pUtil.findFile(file,context.searchPaths, context.global["__MAIN_FILE__"]);
}


/**
 * Convert a macro syntax to a defined render sintaz
 * 
 * @param {string} macro Macro syntax string
 * @return {string} marco rendered on string
 */
Render.prototype.render=function(macro, context ){
  context = this.getContext(context);
  var ret="";
  var macroDef=null;
  forEachMacros(macro,function(name,params,inside){
    ret+=this.execMacro(name,params,inside,context);
  },this);
  return ret;
}

Render.prototype.getContext=function(context) {
  if(!context) {
        context={
          ser: new utl.Serialized(),
          me: this,
          type: this.type,
          searchPaths: this.searchPaths,
          global: {}
        };
        if(typeof(this.globals)=="object") {
          for(var n in this.globals) {
            context.global[n]=this.globals[n];
          }
        }
  }
  return context;
}

/**
 * Function to render to file
 * @param {string} file Output
 * @param {string} body of document to write
 * @param {string} template file to use
 * @param {Serialize} ser Serialized object
 */
Render.prototype.renderToFileImpl=function(file,body,template,context, ser) {
  ee.emit("error",`renderToFileImpl(file,body,template) Not implemented !`);
}
Render.prototype.renderFile=function(macro,file,template,fn) {
  var context=this.getContext();
  var template=this.findFile(template,context);
  var me=this;
  
  if(!template) {
    template=this.findFile(this.template,context);
  }
  
  var body=this.render(macro,context);
  if(context.global.template) {
    var tmp=this.findFile(context.global.template+"."+this.type,context);
    if(tmp) {
      template=tmp;
    } else {
      console.warn(`Template "${context.global.template}.${this.type}" not found on search paths !`);
    }
  }
  if(!template) {
    if(context.template) {
      template=this.findFile(context.template,context);
      if(!template) {
        ee.emit("error",`Template '${context.template}' not found on search paths !`);
      }
    } else {
      ee.emit("error",`Template '${template}' or '${this.template}' not found on search paths !`);
    }
  }
  context.ser.add(function(s){
    me.renderToFileImpl(file,body,template, context, context.ser);
    if(typeof(fn) == "function") {
      context.ser.add(function(s){
        fn();
        s.next()
      });
    }
    s.next();
  });
  context.ser.start();
  /*
	template = this.getTemplatePath(template,"template.html");
	var txt=fs.readFileSync(template).toString();

	txt=txt.replace(new RegExp("\\{\\{\\{BODY\\}\\}\\}","gm"),body);
  fs.writeFileSync(file,txt);
  */
}


/**
 * Scroll all macros 
 * @param {string} string String for scroll all macros
 * @param {function} fn function(name,params,inside)
 * @param {object} ctx COntext for calling a function
 */
function forEachMacros(string,fn, ctx) {
	var cur=null;
	var idx=0
	utl.splitFromRegEx(string,[
    /(\\\{|\\\}|\\\>|\\\<)|(<)?\{\{([^:{}]+?)(:((\\\}|.)+?))?\}\}(>)?/gm
		], (v,id)=>{
      if(!v[1]) {
        if(cur) {
          if(v[3]==cur.name) {
            if(v[2]) {
              cur.level++;
            } else if(v[7]) {
              cur.level--;
            }
            if(cur.level==0) {
              fn.call(ctx,cur.name, cur.params, string.substring(idx,v.index));
              cur=null;
            }
          }
        } else if(v[2]) {
          // start
          cur={name: v[3], params: v[5], level: 1};
          idx=v.index+v[0].length;
        } else if(v[7]) {
          // end
        } else {
          fn.call(ctx,v[3], v[5]);
        }
      }
   });
}



/**
 * Execute a macro
 * @param {object} macroDef Object with information { name: <name>, params: <params>, inside: <inside>}
 */
Render.prototype.execMacro=function(name,params,inside,context) {
  var fn=this.macroFunctions[name];
  if(typeof(fn)=="function") {
    var ret= fn.call(this, params, inside, context);
    if(ret)
      return ret;
  } else {
    // Search on extended defined macros !
    var def=context.extendMacros ? context.extendMacros[name] : null;
    if(def) {
      var type=def.type || "md";
      var value=def.fn.call(this,params,inside,context);
      if(type!="macro") {
        if(context.stileblock) {
          return this.render(mdLib.processStyles(value),context);
        } else {
          return this.render(mdLib.stringToMacro(value),context);
        }  
      } else {
        return this.render(value,context);
      }

    }
    console.warn(`Macro "${name}" is not defined ! params=${params}, inside=${inside}`);
  }
  return "";

}

Render.prototype.rendFN=function(context, cmd, param) {
  var fn=this.rendFunctions[cmd];
  if(typeof(fn)=="function") {
    return fn.call(this,context,param);
  } else {
    var msg=`Render function '${cmd}' not exist ! param=${JSON.stringify(param)}`;
    var obj=this.existRendFunctions[cmd];
    if(obj) {
      msg=`Render function ${cmd}(context,params) not implemented !\nDescription: ${obj.desc}\nParams: ${JSON,stringify(obj,null,"  ")}`;
    }
    console.error(msg);
    this.rendFunctions["error"].call(this,context,msg)
  }
}
/**
 * Macro defined function
 */
Render.prototype.macroFunctions = {
  "globals": function(params,inside,context) {
    try {
      var gStr=mdLib.decode(inside);
      var g=JSON.parse(gStr);
      // Copy all parameters
      for(var n in g) {
        context.global[n]=g[n];
      }
    } catch(e) {
      console.error("Error on setting globals ! error="+e.message);
    }
  },
  "_FILE_": function(params,inside,context) {
    var v=/^(.+)?(:([0-9]+))$/.exec(params);
    return this.rendFN(context,"file",{ file: v[1], row: v[3]});
  },
  "title": function(params,inside,context) {
    return this.rendFN(context,"title",{ level: params, text: this.render(inside,context)});
  },
  "t": function(params,inside,context) {
    if(params=="LAST!") {
      params=context.currentStyle  ;
    }
    context.currentStyle=params;
    return this.rendFN(context,"text",{ style: params, text: mdLib.decode(inside)});
  },
  "p": function(params,inside,context) {
    var ret= this.rendFN(context,"paragraph",this.render(inside,context));
    return ret;
  },
  "hline": function(params,inside,context) {
    return this.rendFN(context,"hline",params);
  },
  "ul": function(params,inside,context) {
    var list=[];
    forEachMacros(inside,(name,params,inside) => {
      if(name=="li") {
        var v=/^([0-9]+)(,(.).*)?$/.exec(params);
        var style=null;
        if(v[3]=="*" || v[3]=="-")
          style=v[3];
        list.push({ level: Number(v[1]), style: v[3], text: this.render(inside,context)});
      } else {
        this.execMacro(name,params,inside,context);
      }
    });
    return this.rendFN(context,"list",list);
  },
  "a": function(params,inside,context) {
    return this.rendFN(context,"anchor",{ id: params, text: this.render(inside,context)});
  },
  "inlinesrc": function(params,inside,context) { 
    return this.rendFN(context,"sourceinline",inside);
  },
  "code": function(params,inside,context) { 
    return this.rendFN(context,"source",{type: params, text: this.render(inside,context)});
  },
  "link": function(params,inside,context) {
    return this.rendFN(context,"link",{url: params, text: this.render(inside,context)});
  },
  "img": function(params,inside,context) { 
    return this.rendFN(context,"img",{url: params, text: this.render(inside,context)});
  },
  "pbreak": function(params,inside,context) { 
    return this.rendFN(context,"pagebreak",null);
  },
  "table": function(params,inside,context) { 
    var table={
      align: params,
      head:[],
      body:[],
    };
    var me=this;
    forEachMacros(inside,(name,params,inside) => {

      if(name=="rowHead" || name=="rowBody") {
        var tableObj=table;
        var row=[];
        forEachMacros(inside,(name,params,inside) => {
          if(name=="td") {
            var v=/^(.)([0-9]+)$/.exec(params)
            row.push({ dim: Number(v[2]), align: (v[1]=="*" && tableObj.align.length>row.length? tableObj.align.substr(row.length,1) :v[1]), text: this.render(inside,context) })
          } else {
            this.execMacro(name,params,inside,context);
          }
          
        });
        if(row.length>0) {
          if(name=="rowHead") {
            tableObj.head.push(row);
          } else {
            tableObj.body.push(row);
          }
        }
      } else {
        this.execMacro(name,params,inside,context);
      }
    });
    delete table.align;
    return this.rendFN(context,"table",table);
  },
  "quoteblock": function(params,inside,context) { 
    var list=[];
    forEachMacros(inside,(name,params,inside) => {
      if(name=="quote") {
        list.push({ level: Number(params), text: this.render(inside,context)});
      } else {
        this.execMacro(name,params,inside,context);
      }
    });
    return this.rendFN(context,"quote",list);
  },
  "toc": function(params,inside,context) { 
    if(Array.isArray(context.global.toc)) {
      var list=[];
      context.global.toc.forEach((e)=>{
        list.push({level: e.level, text: this.render(`<{{link:#${e.id}}}${e.text}{{link}}>`,context)});
      });
      return this.rendFN(context,"list",list);
    }
    return null;
  },
  "now": function(params,inside,context) { 
    var formatNumber=function(number, len) {
      if(!len || len<=0 ) len=2;
      number=String(number);
      while(number.length<len)
      number="0"+number;
      return number;
    }
    var format="Y/M/D h:m:s";
    
    var formatter={
      date: new Date(),
      y: function() {
        return formatNumber(this.date.getFullYear()).substring(2);
      },
      Y: function() {
        return formatNumber(this.date.getFullYear());
      },
      M: function() {
        return formatNumber(this.date.getMonth()+1);
      },
      D: function() {
        return formatNumber(this.date.getDate());
      },
      h: function() {
        return formatNumber(this.date.getHours());
      },
      m: function() {
        return formatNumber(this.date.getMinutes());
      },
      s: function() {
        return formatNumber(this.date.getSeconds());
      },
      ms: function() {
        return formatNumber(this.date.getMilliseconds(),3);
      }
      
    };
    if(params)
    format=params;
    format= format.replace(new RegExp("(ms|y|Y|M|D|h|m|s)","g"),function(c){
      return formatter[c]();
    });
    
    return this.render(`<{{t:LAST!}}${format}{{t}}>`,context);
  },
  "styleblock": function(params,inside,context) { 
    context.stileblock=true;
    var ret=this.render(inside,context);
    context.currentStyle=null;
    context.stileblock=false;
    return ret;
  },
  "var": function(params,inside,context) { 
    if(!context.global.vars) context.global.vars={};
    context.global.vars[params]=inside;
  },
  "out": function(params,inside,context) { 
    var type="md";
    var v=/^(.+?)(,([^,]+))?$/.exec(params);
    if(!v) return this.rendFN(context,"error",`out macro parameters '${params}' is wrong (axpected <varid[,type]?>) !`);
    if(v[3]==="macro") 
      type="macro";
    var value=context.global.vars[v[1]];
    if(type=="md") {
      if(context.stileblock) {
        return this.render(mdLib.processStyles(value),context);
      } else {
        return this.render(mdLib.stringToMacro(value),context);
      }
      
    } else {
      return this.render(value,context);
    }
  },
  "extend": function(params,inside,context) { 
    if(!context.extendMacros) {
      
      context.extendMacros={}
    }
    if(this.macroFunctions[params]){
      return this.rendFN(context,"error",`Extend macro '${params}' already exist a main macro (cannot extend a main macro) !`);
    }
    // console.log(param,inside);
    try {
      var script=new vm.Script(inside);
      context.extendMacros[params]=script.runInThisContext();
      if(typeof(context.extendMacros[params])!="object") {
        return this.rendFN(context,"error",`Extend macro '${params}' wrong javascript object returned !`);
      }
      if(typeof(context.extendMacros[params].fn)!="function") {
        return this.rendFN(context,"error",`Extend macro '${params}' not defined 'fn' function !`);
      }
      
    } catch(e) {
      return this.rendFN(context,"error",`Extend macro '${params}' javascript error: '${e}`);
    }
    return "";
  },
  "comment": function(params,inside,context) { 
    return "";
  },
  "template": function(params,inside,context) { 
    context.global.template=params;
    return "";
  },
  "error": function(params,inside,context) { 
    return this.rendFN(context,"error",params);
  }
}


Render.prototype.error=function(message,context) {
  this.rendFunctions["error"](context,message);
}

Render.prototype.existRendFunctions = {
  "error": {
    "desc": "Add information of error catched on macro syntax document",
    "params": "(string) Error message"
  },
  "file": {
    "desc": "Add information of error on document",
    "params": {file: "{string} file", row: "{int} row"}
  },
  "text": {
    "desc": "Render text element on paragraph",
    "params": { style: "{string} styles name divide of ; separator", text: "{String} inside text"}
  },
  "title": {
    "desc": "Render title element on paragraph",
    "params": { level: "{int} Title level", text: "{String} inside body"}
  },
  "paragraph": {
    "desc": "Render paragraph element",
    "params": {  text: "{String} inside body"}
  },
  "hline": {
    "desc": "Render horizontal line element",
    "params": "{String} 'single' for single style or 'double' for double string"
  },
  "list": {
    "desc": "Render list element",
    "params": [
      {"level":"{int} element level","style":"{string} elment style '*'=circle '-'=square if not exist is without a simbole","text":"{string} inside body"}
    ]
  },
  "anchor": {
    "desc": "Render anchor element",
    "params": {
      "id":"{string} Anchor identifer",
      "text":"{string} inside body"
    }
  },
  "sourceinline": {
    "desc": "Render source in line element",
    "params": "{string} inside body"
  },
  "source": {
    "desc": "Render source element",
    "params": {
      "type":"{string} source type (md, xml, c, c++,...)",
      "text":"{string} source code"
    }
  },
  "link": {
    "desc": "Render link element",
    "params": {
      "url":"{string} link value (if start with # is an inside document anchor)",
      "text":"{string} inside body"
    }
  },
  "img": {
    "desc": "Render img element",
    "params": {"url":"{string} url of image","text":"{string} caption inside body"}
  },
  "pagebreak": {
    "desc": "Render pagebreak element",
    "params": null
  },
  "table": {
    "desc": "Render table element",
    "params": {
      "head: head rows": [
        [
          {
            "dim": "{int} colslan value",
            "align": "{string} Align (L=left, C=center, R=right)",
            "text": "{string} inside cell body"
          }
        ]
      ],
      "body: body rows": [
        [
          {
            "dim": "{int} colslan value",
            "align": "{string} Align (L=left, C=center, R=right)",
            "text": "{string} inside cell body"
          }
        ]
      ]
    }
  },
  "quote": {
    "desc": "Render quote element",
    "params": [
      {"level":"{int} quote level","text":"{string} inside body"}
    ]
  },
}

/**
 * Defined rendered functions 
 */
Render.prototype.rendFunctions = {
  /**
   * Add file information on document
   * @param {object} param: {file: <file>, row: <row>}
   */
  "error": function(context, param) {
    ee.emit("error",new Error(param));
  }
}



module.exports = Render
