const fs=require("fs");
const pUtil=require("./pathUtils.js");
const path=require("path");

/**
 * String array manager
 * @param {string} content 
 */
function StringArray(content) {
	this.type="StringArray";
	this.arr=Array.isArray(content) ? content : [content];
	/**
	 * Convert a array to string extract only string on array and concat it
	 */
	this.toString=function() {
		var ret="";
		this.arr.forEach(function(str) {
			if(typeof(str)=="string")
				ret+=str;
		});
		return ret;
	}
	/**
	 * Get array value
	 */
	this.toArray=function() {
		return this.arr;
	}
	/**
	 * Transform string position to object type
	 * { 
	 * 	idx: array index,
	 *  pos: position on string
	 * }
	 */
	this.getPos=function(pos) {
		var idx=0;
		for(var i=0;i<this.arr.length;i++) {
			var s=this.arr[i];
			if(typeof(s)=="string") {
				if(pos>=idx && pos<=idx+s.length) {
					return { idx: i, pos: pos-idx };
				}
				idx+=s.length;
			}
		}
		return null;
	}

	this.replaceOnArray=function(start, end, val) {
		if(val.type=="StringArray") {
			this.arr.splice(start,end-start+1);
			val.arr.forEach((o)=>{
				this.arr.splice(start++,0,o);
			});
		} else if(Array.isArray(val)) {
			this.arr.splice(start,end-start+1);
			val.forEach((o)=>{
				this.arr.splice(start++,0,o);
			});
		} else {
			this.arr.splice(start,end-start+1,val);
		}
	}
	/**
	 * Replace to an object a array value
	 */
	this.replace=function(start, end, val) {
		pStart = this.getPos(start);
		pEnd= this.getPos(end);
		if(pStart && pEnd) {
			var toAdd=[];
			if(pStart.pos>0) {
				toAdd.push(this.arr[pStart.idx].substring(0,pStart.pos));
			}
			toAdd.push(val);
			if(pEnd.pos < this.arr[pEnd.idx].length) {
				toAdd.push(this.arr[pEnd.idx].substring(pEnd.pos));
			}
			var idx=pStart.idx;
			this.arr.splice(pStart.idx,pEnd.idx - pStart.idx +1,toAdd.shift());
			while(toAdd.length>0) {
				idx++;
				this.arr.splice(idx,0,toAdd.shift());
			}
		}
	}
	/**
	 * Get a block from start to end point
	 */
	this.get=function(start,end) {
		pStart = this.getPos(start);
		pEnd= this.getPos(end);
		var ret=[];
		if(pStart && pEnd) {
			if(pStart.idx!=pEnd.idx) {
				ret.push(this.arr[pStart.idx].substring(pStart.pos))
				for(var i=pStart.idx+1; i<pEnd.idx; i++) {
					ret.push(this.arr[i]);
				}
				ret.push(this.arr[pEnd.idx].substring(0,pEnd.pos));
			} else {
				ret.push(this.arr[pStart.idx].substring(pStart.pos,pEnd.pos));
			}
		}
		
		return this.join(ret);
	}
	/**
	 * Join string array and if is only one string return string
	 */
	this.join = function(array) {
		if(Array.isArray(array)) {
			var idx=0;
			while(idx<array.length) {
				if(typeof(array[idx])!="string") {
					idx++
				} else {
					if(typeof(array[idx+1])=="string") {
						array.splice(idx,1,array[idx]+array[idx+1]);
					} else {
						idx++;
					}
				}
			}
			if(array.length==1 && typeof(array[0])=="string")
				return array[0];
		}
		return array;
	}
	/**
	 * Scroll all element of array and call a fn:
	 * fn: function(o,idx) 
	 * 	return 
	 * 		true for break loop
	 * 		array for 
	 * 
	 */
	this.forEach=function(fn,context) {
		for(var i=0;i<this.arr.length;i++) {
			var o=this.arr[i];
			var ret=fn.call(context,o,i);
			if(typeof(ret)=="boolean" && ret) {
				break;
			}
		}
	}
}

/**
 * Create a macro object
 * @param {string} name macro name
 * @param {string} params Parameters
 * @param {any} inside Inside a macro
 * @param {number} start Strart position
 * @param {number} end End position
 */
function newMacro(name,params,inside, start, end) {
	var ret= { name: name};
	if(params)
		ret.params=params;
	if(inside) {
		if(Array.isArray(inside) && inside.length==1)
			ret.inside=inside[0];
		else 
			ret.inside=inside;
	}
	if(start!=null)
		ret.start=start;
	if(end!=null)
		ret.end=end;
	return ret;
}

function replaceNewline(str) {
	return str.replace(/\n\r|\r\n/gm,"\n").replace(/\r/gm,"\n");
}

/**
 * Read a file and return StringArray with all info of file row
 * @param {string} file Path of file
 */
function loadFile(file) {

	var content=replaceNewline(fs.readFileSync(file).toString()).split("\n");
	var buf=[];
	file=pUtil.toAbsolute(file);
	var row=1;
	content.forEach((l)=>{
		buf.push(newMacro("__file",file+":"+row));
		buf.push(l+"\n");
		row++;
	});

	
	return new StringArray(buf);
}

/**
 * Search file on context search paths
 * @param {string} file File to search
 * @param {object} context Context of macros
 */
function findFile(file,context) {
	if(!path.isAbsolute(file)) {
		var p=pUtil.toAbsolute(path.join(path.dirname(context.stack[context.stack.length-1]),file));
		if(fs.existsSync(p))
			return p;
		return pUtil.findFile(file,context.search);
	}
	return file;
}

function process__file(params,inside, context) {
	if(context.srcstack.length>0) {
		var v=/^(.*?)[:]([0-9]+)$/.exec(params);
		if(v) {
			context.srcstack[context.srcstack.length-1]={file: v[1], row: Number(v[2])};
		}
	}
	return null;
}
var macroDef = {
	"pre": {
		"include": {
			// Task execution time  (pre, post, or default current)
			fn: function(params, inside, context) {
				var f=findFile(params,context);
				if(f) {
					if(context.stack.indexOf(f)>=0)
						return newMacro("error","Included file \""+params+"\" is circular included stack="+context.stack+" !");
					return fileToMacro(f,context,["pre"]);
				} else {

					return newMacro("error","Included file \""+params+"\" not finded !");
				}
			}
		},
		"__file": {
			fn: process__file
		}
	},
	"main": {
		"__file": {
			fn: process__file
		}
	},
	"post": {
		"__file": {
			fn: process__file
		}
	}
}

var mdDef = {
	processMD: function(str,context, ret) {
		var ret=[];
		lines=new LineReader(str);
		var line=null;
		while((line=lines.next())!=null) {
			var def=this.getDef(line);
			if(def) {
				lines.prev();
				ret.push(def.fn.call(this,lines,context,def,ret));
			}
		}
		return ret;
	},
	getDef: function(line) {
		for(var i=0;i<this.defs.length;i++) {
			if(this.defs[i].re.test(line)) {
				return this.defs[i];
			}
		}
		return null;
	},
	processStyles: function(str, context) {
		var ret=[];

		var style=null;
		while((style=this.findStyle(str))) {
			if(style.val.index>0) {
				ret.push(newMacro("span",null,str.substring(0,style.val.index)));
			}
			var value=style.def.fn.call(this,style.val,context);
			if(Array.isArray(value)) {
				if(value.length==1)
					ret=ret.concat(value[0]);
				else
					ret=ret.concat(value);
			} else 
				ret.push(value);
			str=str.substring(style.val.index+style.val[0].length);
		}
		if(str)
			ret.push(newMacro("span",null,str));
		return ret;
	},
	findStyle: function(str) {
		var ret=null;
		for(var i=0;i<this.defStyles.length;i++) {
			var style=this.defStyles[i];
			var v=style.re.exec(str);
			if(v && (!ret || ret.val.index > v.index)) {
				ret={val: v, def: style};
			}
		}
		return ret;

	},
	defStyles: [
		{
			type: "img",
			re: new RegExp("\\!\\[(.+?)\\]\\\"([^\"]+)\\\"","gm"),
			fn: function(v,context) {
				return newMacro("img",v[2],this.processStyles(v[1]))
			}
		},
		{
			type: "id",
			re: new RegExp("\\[(.+?)\\]\\\"([^\"]+)\\\"","gm"),
			fn: function(v,context) {
				return newMacro("id",v[2],this.processStyles(v[1]))
			}
		},
		{
			type: "link",
			re: new RegExp("\\[(.+?)\\]\\(([^)]+)\\)","gm"),
			fn: function(v,context) {
				return newMacro("link",v[2],this.processStyles(v[1]))
			}
		},
		{
			type: "bold",
			re: new RegExp("\\*\\*([\\s\\S]+?)\\*\\*","gm"),
			fn: function(v,context) {
				return newMacro("span","bold",this.processStyles(v[1]))
			}
		},
		{
			type: "italic",
			re: new RegExp("\\/\\/([\\s\\S]+?)\\/\\/","gm"),
			fn: function(v,context) {
				return newMacro("span","italic",this.processStyles(v[1]))
			}
		},
		{
			type: "underline",
			re: new RegExp("\\_\\_([\\s\\S]+?)\\_\\_","gm"),
			fn: function(v,context) {
				return newMacro("span","underline",this.processStyles(v[1]))
			}
		},
		{
			type: "strikethrough",
			re: new RegExp("\\-\\-([\\s\\S]+?)\\-\\-","gm"),
			fn: function(v,context) {
				return newMacro("span","strikethrough",this.processStyles(v[1]))
			}
		},
		{
			type: "inlinesource",
			re: new RegExp("`([\\s\\S]+?)`","gm"),
			fn: function(v,context) {
				return newMacro("span","inlinesource",v[1])
			}
		}
	],
	defs: [
		{
			type: "hline",
			re: new RegExp("^(([-][-][-]+)|[=][=][=]+)$"),
			fn: function(lines,context,me,ret) {
				var lim=lines.limit();
				var v=me.re.exec(lines.next());
				return newMacro("hline", v[2] ? "single": "double",null,lim.start, lim.end);
			}
		
		},
		{
			type: "pagebreak",
			re: new RegExp("^\\^\\^\\^+$"),
			fn: function(lines,context,me,ret) {
				var lim=lines.limit();
				var v=me.re.exec(lines.next());
				return newMacro("pagebreak", null,null,lim.start, lim.end);
			}
		},
		{
			type: "title",
			re: new RegExp("^(#+)[ \t]+(.*)$"),
			fn: function(lines,context,me,ret) {
				var lim=lines.limit();
				var v=me.re.exec(lines.next());
				var level=v[1].length;
				var text=v[2].trim();
				var punt=null;
				var inside=[];
				if(text.substr(0,1)!='-') {
					if(!context.titles) {
						context.titles={
							id: 0,
							levels: [],
							titles: []
						};
					}
					var id="T"+(context.titles.id++);
					while(context.titles.levels.length<level) {
						context.titles.levels.push(0);
					}
					context.titles.levels[level-1]++;
					if(text.substr(0,1)=='#') {
						text=text.substring(1);
						punt="";
						for(var i=0;i<level;i++) {
							if(i>0) punt+=".";
							punt+=String(context.titles.levels[i])
						}
						var in1=[];
						in1.push(newMacro("span","bold",punt));
						in1=in1.concat(this.processStyles(text,context));
						inside.push(newMacro("id",id,in1));
					} else {
						inside.push(newMacro("id",id,this.processStyles(text,context)));
					}
				} else {
					text=text.substr(1).trim();
					inside=inside.concat(this.processStyles(text,context));
				}
				return newMacro("title",String(level),inside,lim.start,lim.end);
			}
		},
		{
			type: "quote",
			re: new RegExp("^(\\>+)[ \t]+(.*)$"),
			fn: function(lines,context,me) {
				var lim=lines.limit();
				var text="";
				var line=null;
				var level=null;
				var inside=[];
				while((line=lines.next())) {
					var def=this.getDef(line);
					if(def.type=="quote") {
						lim.end=lines.limit().start-1;
						var v=def.re.exec(line);
						if(!level || level!=v[1].length) {
							if(level) {
								inside.push(newMacro("quote",level,this.processStyles(text)));
							}
							text=v[2];
							level=v[1].length;
						} else {
							if(text) text+=" ";
							text+=v[2];
						}
					} else {
						
						lines.prev();
						break;
					}
				}
				if(text && level) {
					inside.push(newMacro("quote",level,this.processStyles(text)));
				}
				return newMacro("subs",null,inside,lim.start,lim.end);
			}
		},
		{
			type: "list",
			re: new RegExp("^([ \t]*)([*-])[ \t]+(.*)$"),
			fn: function(lines,context,me) {
				var lim=lines.limit();
				var line=null;
				var level=null;
				var inside=[];
				while((line=lines.next())) {
					var def=this.getDef(line);
					if(def.type=="list") {
						lim.end=lines.limit().start-1;
						var v=def.re.exec(line);
						var level=Number(v[1].replace(/\t/g,"  ")/2).toFixed(0);
						inside.push(newMacro("li",v[2]+level,this.processStyles(v[3])))
					} else {
						lines.prev();
						break;
					}
				}
				return newMacro("list",null,inside,lim.start, lim.end);
			}
		},
		{
			type: "table",
			re: new RegExp("^[|](.*)$"),
			fn: function(lines,context,me) {
				var lim=lines.limit();
				var reLine=/^[|](.*)([\\|])$/;
				var reConf=/^[|]([:]?[-]+[:]?[|])+$/
				var reEndLine=/^.*[|]$/
				var lim=lines.limit();
				var line=null;
				var inside=[];
				var align="";
				var tdType="th"

				var makeRow=function(str,type,context) {
					var ar=str.split(/[|]/gm);
					for(var i=0;i<ar.length;i++) {
						var s;
						while(ar[i+1] && (s=ar[i]) && s.charAt(s.length-1)=="\\") {
							ar.splice(i,2,s.substr(0,s.length-1)+"|"+ar[i+1]);
						}
					}
					var td=[];
					for(var i=0;i<ar.length;i++) {
						if(ar[i]=="-") {
							if(td.length>0)
								td[td.length-1].colspan++;
							
						} else {
							var v=/^(\:)?([\s\S]*)(\:)?$/gm.exec(ar[i]);
							var o={colspan: 1, inside: strToMacro(v[2],context)};
							if(v[1] || v[3]) 
								o.align=(v[1]==v[3] ? "C":(v[1]==":" ? "L" : "R"))
							else 
								o.align="*"
							td.push(o);
						}
					}
					for(var i=0;i<td.length;i++) {
						td.splice(i,1,newMacro(type,td[i].align+td[i].colspan,td[i].inside));
					}
					return newMacro("row",type,td);

				}
				while((line=lines.next())) {
					lim.end=lines.limit().start-1;
					if(reConf.test(line)) {
						tdType="td";
						var l=line.substr(1,line.length-2).split("|");
						l.forEach((str)=>{
							var v1=/([:])?[-]+([:])?/.exec(str);
							align+= (v1[1]==v1[2] ? "C" : (v1[1] ? "L": "R"));
						});
					} else {
						var v=reLine.exec(line);
						if(v) {
							if(v[2]=="|") {
								inside.push(makeRow(line.substring(1,line.length-2),tdType,context));
							} else {
								var totLine=line.substring(1,line.length-1);
								while((line=lines.next())) {
									lim.end=lines.limit().start-1;
									var c=line.charAt(line.length-1);
									if(c=="\\") {
										totLine+="\n"+line.substring(0,line.length-1);
									} else if(c=="|") {
										totLine+="\n"+line.substring(0,line.length-1);
										inside.push(makeRow(totLine,tdType,context));
										break;
									} else {
										return newMacro("error","Table row not ended !",null,lim.start, lim.end);
									}	
								}

							}
						} else {
							lines.prev();
							break;
						}
					}
				}
				return newMacro("table",align,inside,lim.start, lim.end);
				
			}
		},
		{
			type: "paragraph",
			re: new RegExp("^.*[\\S].*$"),
			fn: function(lines,context,me) {
				var lim=lines.limit();
				var text="";
				var line=null;
				while((line=lines.next())) {
					var def=this.getDef(line);
					if(def.type=="paragraph") {
						if(text) text+=" ";
						text+=line;
						
						lim.end=lines.limit().start-1;
						if(line.substr(line.length-1)==".")
							break;
					} else {
						lines.prev();
						break;
					}
				}
				return newMacro("p", null,this.processStyles(text,context),lim.start, lim.end);
			}

		}
	]
};


/**
 * Process macro of a specific task (pre,main or post)
 * @param {string} task Task to process (pre,main,post)
 * @param {StringArray} ret StringArray for replace macros
 * @param {object} context Context object
 */
function processTask(task,ret,context) {
	var processed=false;
	do {
		processed=false;
		ret.forEach((o,idx)=>{
			if(typeof(o)=="object") {
				var def=macroDef[task] ? macroDef[task][o.name] :  null;
				if(def) {
					var val=def.fn(o.params,o.inside, context);
					if(val!=null) {
						processed=true;
						ret.replaceOnArray(idx,idx,val);
					}		
				}
			}
		},this);
	} while(processed);
	
}

var defaultContext={
	type: "Context",
	/** 
	 * search: Search path
	 */
	search: [pUtil.toAbsolute(__dirname+"/../libmd")]
}

/**
 * Object for handlind a read line for line of text
 * @param {string} str content string
 */
function LineReader(str) {
	this.str=str;
	this.lines=[0];
	var pos=0;
	while((pos=this.str.indexOf("\n",pos))>0) {
		pos++;
		this.lines.push(pos);
	}
	this.current=0;
	
	this.get=function() {
		if(this.current<this.lines.length) {
			var limit=this.limit();
			if(limit) {
				var str=this.str.substring(limit.start,limit.end);
				return str.replace(/[\n]$/,"");
			}
		}
		return null;
	}

	this.next=function() {
		if(this.current<this.lines.length) {
			var ret=this.get();
			this.current++;
			return ret;
		}
		return null;
	}

	this.prev=function() {
		if(this.current>0) {
			this.current--
			return this.get();
		}
		return null;
	}
	this.limit=function() {
		if(this.current>=0 && this.current<this.lines.length) {
			return {start: this.lines[this.current], end: this.lines[this.current+1] ? this.lines[this.current+1]: this.str.length};
		}
		if(this.current>=this.lines.length)
			return {start: this.str.length+1};
		return null;
	}

}

function strToMacro( str, context, execTasks) {
	if(!context)  {
		context = JSON.parse(JSON.stringify(defaultContext));
		context.stack=[];
		context.srcstack=[];
	}
	var ret=new StringArray(str);
	ret= toMacro(ret,context,execTasks);
	return ret;
}

function fileToMacro(file, context, execTasks) {
	if(!context)  {
		context = JSON.parse(JSON.stringify(defaultContext));
		context.stack=[];
		context.srcstack=[];
	}
	file=pUtil.toAbsolute(file);
	context.stack.push(file);
	var ret=loadFile(file);
	
	ret= toMacro(ret,context,execTasks);
	context.stack.pop();
	return ret;
}

/**
 * Convert a file markdown to macro array
 * @param {StringArray} ret File to load
 * @param {object} context Context object
 * @param {array} execTasks execution tasks
 */
function toMacro( ret, context, execTasks) {
	
	if(!execTasks)
		execTasks=["pre","main","post"];

	
	context.srcstack.push(null);
	// Execute replace of source multiline
	var re=/\{\{([#/])([^:]+?)(\:(.*?))?}}|^(```)$|^(```)(.*?)$/mg;
	var a=[];

	var str=ret.toString();
	while((v=re.exec(str))) {
		a.push(v);
	}

	var toReplace=[];
	var idx=0;
	while(idx<a.length) {
		var f=a[idx];
		if(!f[1]) {
			var start=f.index
			var startInt=f.index+f[0].length;
			idx++;
			while(idx<a.length) {
				var f=a[idx];
				if(!f[1]) {
					// is end
					var end=f.index+f[0].length;
					var endInt=f.index;
					toReplace.push(newMacro("source",null,ret.get(startInt,endInt),start,end));
					break;
				}
				idx++
			}
		} else {
			// Block macro
			var start=f.index;
			var startInt=f.index+f[0].length;
			var name=f[2];
			var params=f[4];
			var level=1;
			idx++;
			while(level>0 && idx<a.length) {
				var f=a[idx];
				if(f[1]) {
					if(f[2]==name) {
						level+=f[1]=="#" ? 1 : -1;
						if(level==0) {
							var end=f.index+f[0].length;
							var endInt=f.index;
							toReplace.push(newMacro(name,params,ret.get(startInt,endInt),start,end));
							break;
						}
					}
				}
				idx++;
			}
		}
		idx++;
	}
	while((o=toReplace.pop())) {
		ret.replace(o.start,o.end,newMacro(o.name, o.params,o.inside));
	}

	// Eseguo il replace di tutte le macro (single line)
	var reSingle=/\{\{([^#/][^:]*?)(:(.*?))?\}\}/gm;
	str=ret.toString();
	while((o=reSingle.exec(str))) {
		toReplace.push(newMacro(o[1],o[3],null,o.index, o.index+o[0].length));
	}
	while((o=toReplace.pop())) {
		ret.replace(o.start,o.end,newMacro(o.name, o.params,o.inside));
	}
	// Process a markdown syntax
	toReplace=mdDef.processMD(ret.toString(),context,ret);
	while((o=toReplace.pop())) {
		ret.replace(o.start,o.end,newMacro(o.name, o.params,o.inside));
	}

	// Execute macros
	if(execTasks.indexOf("pre")>=0)
		processTask("pre",ret,context);
	if(execTasks.indexOf("main")>=0)
		processTask("main",ret,context);
	if(execTasks.indexOf("post")>=0)
		processTask("post",ret,context);
	
	context.srcstack.pop();
	return ret.arr;

}

module.exports = {
	toMacro: function(file,context) {
		var ret=fileToMacro(file,context);
		fs.writeFileSync("mark-out.json",JSON.stringify(ret,null,"  "));
		return ret;
	},
	defaultContext: defaultContext
}