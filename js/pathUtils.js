const path=require("path");
const fs=require("fs");

module.exports = {
	toAbsolute: function(file) {
		if(!path.isAbsolute(file)) {
			file=path.resolve(file);
		}
		return path.normalize(file).replace(/\\/g,"/");
	},
	findFile: function(file, dirs, parentFile) {
		if(!file)
			return null;
		if(!path.isAbsolute(file)) {
			var f=null;
			if(parentFile) {
				f=path.dirname(parentFile)+"/"+file;	
				if(fs.existsSync(f))
					return this.toAbsolute(f);
			}
			for(var i=0; i < dirs.length;i++) {
				f=dirs[i]+"/"+file;
				if(fs.existsSync(f))
					return this.toAbsolute(f);
			}
			
		}
		if(fs.existsSync(file))
			return this.toAbsolute(file);
		return null;
	},
	findFileFromParent: function(file, parent) {
		if(!path.isAbsolute(file)) {
			file=path.dirname(parent)+"/"+file;
		}
		if(fs.existsSync(file))
			return this.toAbsolute(file);
		return null;
	}
	
}