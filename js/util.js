var fs = require('fs');
const path = require('path');
var JSZip = require("jszip");

/**
 * 
 * @param {string} string String to split
 * @param {array} re Array of regular expressions
 * @param {function} fn Function with call with params (v,id) reg exp exec result and index on reg exprs
 */
function splitFromRegEx(string,re,fn) {
	var finded=[];
	
	re.forEach((r,id)=>{
		var v=null;
		while((v=r.exec(string))) {
			finded.push({id: id, v: v});
		}
	});
	finded.sort((a,b)=>{
		if(a.v.index<b.v.index) {
			return -1;
		} else if(a.v.index>b.v.index) {
			return 1;
		}
		if(a.id<b.id)
			return -1;
		if(a.id>b.id)
			return 1;
		return 0;
		
	});
	var idx=0;
	finded.forEach((o)=>{
		if(o.v.index>=idx) {
			idx=o.v.index+o.v[0].length;
			fn(o.v,o.id);
		} 
	});
}

function getAllFiles(path) {
	var ret=[];
	if( fs.existsSync(path) ) {
	  fs.readdirSync(path).forEach(function(file,index){
		var curPath = path + "/" + file;
		if(fs.lstatSync(curPath).isDirectory()) { // recurse
		  var dirRet=getAllFiles(curPath);
		  dirRet.forEach(function(file) {
			ret.push(file);
		  });
		} else { // delete file
		  ret.push(curPath);
		}
	  });
	}
	return ret;
  };

function mkDir(dir) {
	if(!fs.existsSync(dir)) {
		var dir=path.normalize(dir);		
		var cur="";
		dir.split(/\\|\//g).forEach((sub)=>{
			if(cur) cur+="/";
			cur+=sub;
			if(!fs.existsSync(cur)) {
				fs.mkdirSync(cur);
			}
		});
	}
}
	

function zipDirectoryTo(dir,fileOut,endFN) {
	var files=getAllFiles(dir);
	var zipWriter = new JSZip();
	var dirs={"": zipWriter};
	files.forEach((f)=>{
		  var fileLocalPath=f.substring(dir.length+1);
		  var localDir="";
		  if(fileLocalPath.indexOf("/")>0) {
			localDir=fileLocalPath.substring(0,fileLocalPath.lastIndexOf("/"));
			fileLocalPath=fileLocalPath.substring(fileLocalPath.lastIndexOf("/")+1);
			dirs[localDir]=zipWriter.folder(localDir);
		  }
		  //console.log(f);
		  dirs[localDir].file(fileLocalPath,fs.readFileSync(f));
	  });
	  zipWriter.generateNodeStream({type:'nodebuffer',streamFiles:true}).pipe(fs.createWriteStream(fileOut)).on('finish', function () {
					if(endFN)
						endFN();
			  });
}

/**
 * Unzip file to a specific directory
 * @param {string} zipfile Path of zip file
 * @param {string} destinationDir destination directory
 */
function unzipToDirectory(zipfile, destinationDir, fn, context) {
	var zip = new JSZip();
	var ser=new Serialized(this);
	zip.loadAsync(fs.readFileSync(zipfile))
	.then(function (zip) {
			// will be called, even if content is corrupted
			Object.keys(zip.files).forEach(function (filename) {
				var o=zip.files[filename];
				if(o.dir) {
					
					mkDir(destinationDir+"/"+filename);
				} else {
					var file=destinationDir+"/"+filename;
					ser.add(function(me){
						// is a file
						o.async("nodebuffer").then(function(buf) {
							
							fs.writeFileSync(file,buf);
							me.next();
						});

					})
					
				}
			});
			ser.add(function(me){
				// End of unzip
				fn.call(context);
			})
			ser.start();
	}, function (e) {
			// won't be called
			console.error(e);
	});

}


function deleteFolderRecursive(path) {
	if( fs.existsSync(path) ) {
	  fs.readdirSync(path).forEach(function(file,index){
		var curPath = path + "/" + file;
		if(fs.lstatSync(curPath).isDirectory()) { // recurse
		  deleteFolderRecursive(curPath);
		} else { // delete file
		  fs.unlinkSync(curPath);
		}
	  });
	  fs.rmdirSync(path);
	}
};


/**
 * 
 * @param {object} context 
 */
function Serialized( context) {
	this.current=0;
	this.functions=[]
	this.context=context ? context: this;
}

Serialized.prototype.add=function(fn, context) {
	if(typeof(fn)=="function") {
		if(context) {
			this.functions.push({fn: fn, context: context});
		} else {

			this.functions.push(fn);
		}
	} else if(Array.isArray(fn)) {
		for(var i=0;i<fn.length;i++)
			this.add(fn[i]);
	}
}

Serialized.prototype.execCurrent=function() {
	if(this.current<this.functions.length) {
		var fn=this.functions[this.current];
		if(typeof(fn)=="function") {
			fn.call(this.context,this);
		} else {
			fn.fn.call(fn.context,this);
		}
	}
}
Serialized.prototype.start=function() {
	this.current=0;
	this.execCurrent();
}

Serialized.prototype.next=function() {
	this.current++;
	this.execCurrent();
	
}


  
 

module.exports = {
	splitFromRegEx: splitFromRegEx,
	zipDirectoryTo: zipDirectoryTo,
	unzipToDirectory: unzipToDirectory,
	removeDir: deleteFolderRecursive,
	getAllFiles: getAllFiles,
	Serialized: Serialized,
	mkdir: mkDir
}