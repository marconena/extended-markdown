var Rend=require("./rend");
const fs=require("fs");
const path=require("path");

function RendImpl(searchPath) {
  Rend.call(this,searchPath);
}

RendImpl.prototype = Object.create(Rend.prototype);

RendImpl.prototype.type="html";
RendImpl.prototype.template="template.html";

RendImpl.prototype.renderToFileImpl=function(file,body,template,context) {
  var txt=fs.readFileSync(template).toString();
  txt=txt.replace(new RegExp("\\{\\{\\{BODY\\}\\}\\}","gm"),body);
  fs.writeFileSync(file,txt);
}

RendImpl.prototype.rendFunctions["error"]=function(context, param) {
  console.error(new Error(param));
}
RendImpl.prototype.rendFunctions["file"]=function(context, param) {
  return "";
}

RendImpl.prototype.rendFunctions["text"]=function(context, param) {
  if(param.style) {
    var ret="<span class=\""+param.style.replace(/;/g," ")+"\">";
    ret+=this.encode(param.text)
    ret+="</span>";
    return ret;
  } 
  return this.encode(param.text);
}
RendImpl.prototype.rendFunctions["title"]=function(context, param) {
  return "<h"+param.level+">"+param.text+"</h"+param.level+">\n";
}

RendImpl.prototype.rendFunctions["paragraph"]=function(context, param) {
  return "<div>"+param+"</div>\n";
}
RendImpl.prototype.rendFunctions["hline"]=function(context, param) {
  return "<hr "+(param ? "class=\""+param+"\" ":"")+"/>\n";
}
RendImpl.prototype.rendFunctions["list"]=function(context, param) {
  
  var types={"*":"circle","-": "square"};
  var curLevel=0;
  var ret="";
  if(Array.isArray(param)) {
    param.forEach((li)=>{
      var lev=Number(li.level);
      if(lev!=curLevel) {
        ret+= (lev>curLevel ? "<ul>\n": "</ul>\n").repeat(Math.abs(lev-curLevel));
      }
      curLevel=lev;
      ret+='<li class="'+(types[li.style] ? types[li.style]: "none")+'" >';
      ret+=li.text;
      ret+="</li>\n";
    });
    ret+= "</ul>\n".repeat(curLevel);
  }
  return ret;
}
RendImpl.prototype.rendFunctions["anchor"]=function(context, param) {
  
  return `<span id="${param.id}" >${param.text}</span>`;
}
RendImpl.prototype.rendFunctions["sourceinline"]=function(context, param) {
  return `<code class="inline" >${param}</code>`;
}
RendImpl.prototype.rendFunctions["source"]=function(context, param) {
  return `<code ${(param.type ? ` lang="${param.type}"`: "")}">${param.text}</code>\n`;
}
RendImpl.prototype.rendFunctions["link"]=function(context, param) {
  var cls="extarnal";
  if(param.url.substring(0,1)=="#")
  cls="internal";
  return `<a href="${param.url}" class="${cls}" >${param.text}</a>`;
}
RendImpl.prototype.rendFunctions["img"]=function(context, param) {
  var file=this.findFile(param.url,context);
	if(file) {
    var ret="<img src=\"data:image/";
		ret+=path.extname(file).substring(1);
		ret+=";base64,";
		ret+=fs.readFileSync(file).toString('base64');
		ret+="\" title=\""
		ret+=this.encode(param.text)
		ret+="\" />"
	} else {
    ret=`<img src="${param.url}+" title="${this.encode(param.text)}" />`;
	}
  
	return ret;
  return `<img />`;
}
RendImpl.prototype.rendFunctions["pagebreak"]=function(context, param) {
  return `<div class="pagebreak" style="page-break-after: always;" />\n`;
}
RendImpl.prototype.rendFunctions["table"]=function(context, table) {
  var renderTableRow=function(row,tag) {
    var ret="<tr>\n";
    row.forEach((td)=>{
      ret+="<"+tag
      ret+=" style=\"text-align: ";
      if(td.align=="L") {
        ret+="left";
      } else if(td.align=="R") {
        ret+="right";
      } else {
        ret+="center";
      }
      ret+="\""
      if(td.dim>1) {
        ret+="colspan=\"";
        ret+=td.dim;
        ret+="\"";
      }
      ret+=" >";
      ret+=td.text;
      ret+="</"+tag+">\n";
    });
    ret+="</tr>\n";
    return ret;
  }
  
  var ret="<table>\n"
  if(table.head.length>0) {
    ret+="<thead>\n"
    table.head.forEach((row)=>{
      ret+=renderTableRow(row,"th");
    });
    ret+="</thead>\n"
  }
  if(table.body.length>0) {
    ret+="<tbody>\n"
    table.body.forEach((row)=>{
      ret+=renderTableRow(row,"td");
    });
    ret+="</tbody>\n"
  }
	ret+="</table>\n"
	return ret;
}

RendImpl.prototype.rendFunctions["quote"]=function(context, param) {
  var curLevel=0;
  var ret="";
  if(Array.isArray(param)) {
    param.forEach((q)=>{
      var lev=q.level;
      if(lev!=curLevel) {
        ret+= (lev>curLevel ? "<blockquote>\n": "</blockquote>\n").repeat(Math.abs(lev-curLevel));
      }
      curLevel=lev;
      ret+=q.text;
      ret+="\n";
    });
    ret+= "</blockquote>\n".repeat(curLevel);
  }
  return ret;
}


var htmlChars = {
	'¢' : 'cent',
  '£' : 'pound',
  '¥' : 'yen',
  '€': 'euro',
  '©' :'copy',
  '®' : 'reg',
  '<' : 'lt',
  '>' : 'gt',
  '"' : 'quot',
  '&' : 'amp',
  '\'' : '#39'
};
var reHtmlChars=new RegExp("["+Object.keys(htmlChars).join()+"\u00A0-\u9999]","gim");

RendImpl.prototype.encode=function(text) {
  return text.replace(reHtmlChars,function(c) {
    if(htmlChars[c])
		  return "&"+htmlChars[c]+";";
    return '&#'+c.charCodeAt(0)+';';
	});
}


module.exports = RendImpl;