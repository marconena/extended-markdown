var Rend=require("./rend");
const fs=require("fs");
const path=require("path");
var imageinfo = require('imageinfo');
var utl=require("../util.js");
var http=require('http');
var https=require('https');


function RendImpl(searchPath) {
  Rend.call(this,searchPath);
}

RendImpl.prototype = Object.create(Rend.prototype);

RendImpl.prototype.type="odt";
RendImpl.prototype.template="template.odt";


RendImpl.prototype.getRendContext=function(context) {
  if(context) {
    if(!context.global.odt)
      context.global.odt={ img: [] }
    return context.global.odt;
  }
}

/**
 * Render a template to file
 */
RendImpl.prototype.renderToFileImpl=function(file,body,template,context,ser) {
  // Execute a draw of template
  var tmp = require('tmp');
  var ctx=this.getRendContext(context);
  var tmpdir = tmp.dirSync().name;
  ser.add((s)=>{
    utl.unzipToDirectory(template,tmpdir, function() {
      s.next();
    });  
  })
  ser.add(function(s) {
    var context=fs.readFileSync(tmpdir+"/content.xml").toString();
    fs.writeFileSync(tmpdir+"/content.xml",context.replace("{{{BODY}}}",body));
    s.next();
  });
  var manifest="";
  ctx.img.forEach((img)=>{
    
      ser.add(function(s){
        if(img.file) {
          utl.mkdir(path.dirname(tmpdir+"/"+img.ref));
          fs.writeFileSync(tmpdir+"/"+img.ref,fs.readFileSync(img.file));
          var ext=/\.([^.]+)$/.exec(img.ref)[1];
          manifest+=`<manifest:file-entry manifest:full-path="${img.ref}" manifest:media-type="image/${ext}"/>`
          s.next(); 
        } else {
          var h=http;
          if(img.url.indexOf("https")==0){

            h=https;
          }
          h.get(img.url, (res) => {
            var data = [];
            res.on('data', function(chunk) {
                data.push(chunk);
            }).on('end', function() {
              var buffer = Buffer.concat(data);
              var v=new RegExp("^image\\/([a-z]+).*$","i").exec(res.headers['content-type']);
              if(v) {
                utl.mkdir(path.dirname(tmpdir+"/"+img.ref));
                fs.writeFileSync(tmpdir+"/"+img.ref,buffer);
                var ext=v[1];
                manifest+=`<manifest:file-entry manifest:full-path="${img.ref}" manifest:media-type="image/${ext}"/>`
              }
              s.next();
            }).on("error", function(err) {
              console.error(err);
              s.next();
            });
          });
        }
      });
  });
  // Add manifest
  
  ser.add(function(s) {
    if(manifest) {
      var context=fs.readFileSync(tmpdir+"/META-INF/manifest.xml").toString();
      context=context.replace( /<\/manifest:manifest>/, function(str){
        return manifest+str;
      });
      fs.writeFileSync(tmpdir+"/META-INF/manifest.xml",context);
    }
    s.next()
  })
  
  ser.add(function(s) {
    utl.zipDirectoryTo(tmpdir,file,()=>{
      console.log("Created: "+file);
      s.next();
    })
  });
  ser.add((s)=>{
    utl.removeDir(tmpdir);
    s.next();
  });
  
}


RendImpl.prototype.paragraph=function(text, type) {
  if(!type)
    type="P1";
  return `<text:p text:style-name="${type}">${text}</text:p>`;
}
RendImpl.prototype.span=function(text, style) {
  return `<text:span ${style ? `text:style-name="${style}" `: ""}>${this.encode(text)}</text:span>`;
}


RendImpl.prototype.rendFunctions["error"]=function(context, param) {
  console.error(new Error(param));
}
RendImpl.prototype.rendFunctions["file"]=function(context, param) {
  return "";
}

RendImpl.prototype.rendFunctions["text"]=function(context, param) {
  return this.span(param.text,param.style);
}
RendImpl.prototype.rendFunctions["title"]=function(context, param) {
  return this.paragraph(param.text,`T${param.level}`);
}

RendImpl.prototype.rendFunctions["paragraph"]=function(context, param) {
  return this.paragraph(param);
}
RendImpl.prototype.rendFunctions["hline"]=function(context, param) {
  if(param=="double") {
    return this.paragraph("","HDLINE");
  }
  return this.paragraph("","HLINE");
}
RendImpl.prototype.rendFunctions["list"]=function(context, param) {
  var styles={"*": "Circle","-": "Square", ".":"None"};
  var ret="";
  var last=null;
  var me=this;
  param.forEach(function(ele) {
    if(last && last.style!=ele.style) {
      ret+=`</text:list-item></text:list>`.repeat(last.level);
      last=null;
    }
    var lev=(last ? last.level : 0 );
    if(lev<ele.level) {
      ret+=`<text:list text:style-name="list${styles[ele.style] ? styles[ele.style]: "None" }"><text:list-item>`.repeat(ele.level-lev);
    } else if(lev>ele.level) {
      ret+=`</text:list-item></text:list>`.repeat(lev-ele.level);
    } else {
      ret+=`</text:list-item>`
      ret+=`<text:list-item>`
    }
    
    ret+=me.paragraph(ele.text);

    last=ele;
  });
  if(last) {
    ret+=`</text:list-item></text:list>`.repeat(last.level);
  }
  return ret;
}
RendImpl.prototype.rendFunctions["anchor"]=function(context, param) {
  return `<text:bookmark-start text:name="${param.id}"/>${param.text}<text:bookmark-end text:name="${param.id}"/>`;
}
RendImpl.prototype.rendFunctions["sourceinline"]=function(context, param) {
  return this.span(param,"CODEINLINE");
}
RendImpl.prototype.rendFunctions["source"]=function(context, param) {
  var ret="";
  var idx=0;
  var prefix="";
  var me=this;
  utl.splitFromRegEx(param.text,[ /<text:span [^>]*?>[^<]*[\n][^<]*<\/text:span>/gm ], function(v,id){
    var pos=v[0].indexOf("\n");
    while(pos>=0) {
      var txt=prefix+param.text.substring(idx,v.index+pos);
      prefix="";
      if(txt.indexOf("<text:span")==-1)
        txt=`<text:span >`+txt;
      ret+=me.paragraph(txt+"</text:span>","source");
      idx=v.index+pos+1;
      pos=v[0].indexOf("\n",pos+1);
    }
    prefix='<text:span>';
  })
  if(idx<param.text.length) {
    ret+=me.paragraph(prefix+param.text.substring(idx),"source")
  }
  ret=ret.replace(/(<text:span[^>]*>)([\s\S]*?)(<\/text:span>)/g, function(s0,s1,s2,s3){
    var find=false;
    //console.log(`"${s2}"`);
    s2=s2.replace(/\ \ |[\t]/gm, function(s){
      find=true;
      return '<text:s text:c="2"/>'
    });
    if(find) return s1+s2+s3;
    return s0;
  });
  return ret;
}
RendImpl.prototype.rendFunctions["link"]=function(context, param) {
  return `<text:a xlink:type="simple" xlink:href="${param.url}" text:style-name="Internet_20_link" text:visited-style-name="Visited_20_Internet_20_Link">${param.text}</text:a>`;
}
RendImpl.prototype.rendFunctions["img"]=function(context, param) {
  var ctx = this.getRendContext(context);

  info={width: 320, height: 200};
  var id="Image"+ctx.img.length;
  var ref="Pictures/"+id+/\.[^.]+$/.exec(param.url)[0];
  if(path.isAbsolute(param.url)) {
		ctx.img.push({file: param.url, id: id, ref: ref});
    if(fs.existsSync(param.url)) {
      var data=fs.readFileSync(param.url);
      info = imageinfo(data);
    }
	} else {
		ctx.img.push({url: param.url, id: id, ref: ref});
  }
	
	//console.log(info);
	// var ret="<draw:frame draw:style-name=\"fr1\" draw:name=\"Image"+this.picIdx+"\" text:anchor-type=\"as-char\" svg:width=\"2.5626in\" svg:height=\"2.1354in\" draw:z-index=\""+this.picIdx+"\">"
	var ret="<draw:frame draw:style-name=\"fr1\" draw:name=\"Image"+id+"\" text:anchor-type=\"as-char\" draw:z-index=\"1\" svg:width=\""+info.width+"px\" svg:height=\""+info.height+"px\" >"
	ret += "<draw:image xlink:href=\""+ref+"\" xlink:type=\"simple\" xlink:show=\"embed\" xlink:actuate=\"onLoad\"/></draw:frame>"
	this.picIdx++;
	return ret;
}
RendImpl.prototype.rendFunctions["pagebreak"]=function(context, param) {
  return `<text:p text:style-name="Text_20_body"/>`
}
RendImpl.prototype.rendFunctions["table"]=function(context, table) {
  var len=1;
  var rendRow=function(row,style) {
    var ret=`<table:table-row>`;
    row.forEach((td)=>{
      var align="0";
      if(td.align=="C") align="1"
      if(td.align=="R") align="2"
      ret+=`<table:table-cell ${td.dim>1 ? `table:number-columns-spanned="${td.dim}" `: ""}office:value-type="string">`
      ret+=td.text.replace(/^(<text:p text:style-name=")[^"]+(">)/, function(s,s1,s2) {
        return s1+style+align+s2;
      });
      ret+=`</table:table-cell>`
    });
    ret+=`</table:table-row>`;
    return ret;
  }
  table.body.forEach((o)=>{
    if(o.length>len) len=o.length;
  });
  var ret=`<table:table table:name="Tabella${new Date().getTime()}" ><table:table-column table:number-columns-repeated="${len}"/>`;
  ret+=`<table:table-header-rows>`;
  table.head.forEach((o)=>{
    ret+=rendRow(o,"THEAD.");
  });
  ret+=`</table:table-header-rows>`;
  table.body.forEach((o)=>{
    ret+=rendRow(o,"TBODY.");
  });
  ret+=`</table:table>`;
  return ret;

}

RendImpl.prototype.rendFunctions["quote"]=function(context, param) {
  var ret="";
  var me=this;
  param.forEach((ele)=>{
    ret+=me.paragraph(ele.text,"QUOTE"+ele.level);
  });
  return ret;
}


var htmlChars = {
	'¢' : 'cent',
  '¥' : 'yen',
  '€': 'euro',
  '©' :'copy',
  '®' : 'reg',
  '<' : 'lt',
  '>' : 'gt', 
  '"' : 'quot',
  '&' : 'amp',
  '\'' : '#39'
};

var reHtmlChars=new RegExp("["+Object.keys(htmlChars).join()+"\u00A0-\u9999]","gim");

RendImpl.prototype.encode=function(text) {
  return text.replace(reHtmlChars,function(c) {
    if(htmlChars[c])
		  return "&"+htmlChars[c]+";";
    return '&#'+c.charCodeAt(0)+';';
	});
}


module.exports = RendImpl;
