var Rend=require("./rend");
const fs=require("fs");
const path=require("path");
var imageinfo = require('imageinfo');
var utl=require("../util.js");
var http=require('http');
var https=require('https');


function RendImpl(searchPath) {
  Rend.call(this,searchPath);
}

RendImpl.prototype = Object.create(Rend.prototype);

RendImpl.prototype.type="docx";
RendImpl.prototype.template="template.docx";


RendImpl.prototype.getDocxContext=function(context) {
  if(context) {
    if(!context.global.docx)
      context.global.docx={refId:0 , refs: [], images: [], imageId: 0}
    return context.global.docx;
  }
}

/**
 * Render a paragraph
 * @param {text} text Inside text
 * @param {string} cls Class to use
 */
function drawParagraf(text,cls) {
	return "<w:p>"+(cls ? "<w:pPr><w:pStyle w:val=\""+cls+"\" /></w:pPr>" : "")+text+"</w:p>";
}

/**
 * Render a text inside a paragraph
 * @param {string} text text to render
 * @param {string} type text style
 */
function formatSpan(text,type) {
  if(type) {

    type=type.split(";");
    type=type[type.length-1];
  }
	return "<w:r>"+(type ? "<w:rPr><w:rStyle w:val=\""+type+"\"/></w:rPr>":"")+"<w:t xml:space=\"preserve\" >"+text+"</w:t></w:r>";
}

/**
 * Render a template to file
 */
RendImpl.prototype.renderToFileImpl=function(file,body,template,context,seq) {
  // Execute a draw of template
  var tmp = require('tmp');
  var ctx=this.getDocxContext(context);
  var tmpobj = tmp.dirSync();

  var refContext="";
  var extensions={};
  var links="";
  ctx.refs.forEach((o)=>{
    //{type: "link", val: url, id: id}
    if(o.type=="link") {
      links+="<Relationship Id=\""+o.id+"\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink\" Target=\""+o.val+"\" TargetMode=\"External\"/>";
    }
  });

  
  seq.add(function(s){
    utl.unzipToDirectory(template,tmpobj.name, function() {
      s.next();
    });

  });
  
  seq.add(function(s){
    // When is unzipped
    var fileContent="/word/document.xml";
    var context=fs.readFileSync(tmpobj.name+fileContent).toString();
    fs.writeFileSync(tmpobj.name+fileContent,context.toString().replace("{{{BODY}}}",body));
  
    refContext=fs.readFileSync(tmpobj.name+"/word/_rels/document.xml.rels").toString();
    s.next();

    
  });
  
  ctx.images.forEach((o)=>{
    seq.add(function(s){
        // {url: file, id: id}
        if(o.file) {
          
          var dest="media/"+o.id+path.extname(o.file);
          utl.mkdir(tmpobj.name+"/word/media");
          fs.writeFileSync(tmpobj.name+"/word/"+dest,fs.readFileSync(o.file));
          links+="<Relationship Id=\""+o.id+"\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/image\" Target=\""+dest+"\"/>";
          var ext=path.extname(o.file).substr(1);
          extensions[ext]="image/"+ext;
          s.next();
        } else if(o.url) {
          var h=http;
          if(o.url.indexOf("https")==0)
          h=https;
          h.get(o.url, (res) => {
            var data = [];
            
            res.on('data', function(chunk) {
              data.push(chunk);
            }).on('end', function() {
              var buffer = Buffer.concat(data);
              //console.log(buffer.toString('base64'));
              //  console.log(res.headers['content-type']);
              var v=new RegExp("^image\\/([a-z]+).*$","i").exec(res.headers['content-type']);
              if(v) {
                var ext="."+v[1];
                var dest="media/"+o.id+ext;
                utl.mkdir(tmpobj.name+"/word/media");
                fs.writeFileSync(tmpobj.name+"/word/"+dest,buffer);
                links+="<Relationship Id=\""+o.id+"\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/image\" Target=\""+dest+"\"/>";
                ext=ext.substring(1);
                extensions[ext]="image/"+ext;
              }
              s.next();
            }).on("error", function(err) {
              console.error(err);
              s.next();
            });
          });
        }
     });
   });
    
  seq.add(function(s) {
    refContext=refContext.replace(new RegExp("<\\/Relationships>","gm"),function(txt){
      return links+txt;
    });
    fs.writeFileSync(tmpobj.name+"/word/_rels/document.xml.rels",refContext);
    var ext="";
    for(var e in extensions) {
      ext+=`<Default Extension="${e}" ContentType="image/${e}"/>`
    }
    if(ext) {
      var fPath=tmpobj.name+"/[Content_Types].xml"
      var str=fs.readFileSync(fPath).toString();
      str=str.replace(/<Types .*?>/,function(str1){
        return str1+ext;
      })
      fs.writeFileSync(fPath,str);

    }
    utl.zipDirectoryTo(tmpobj.name,file, function() {
      utl.removeDir(tmpobj.name);
      console.log(file+": End writing....");
      s.next();
    });
  });
    
}

RendImpl.prototype.rendFunctions["error"]=function(context, param) {
  console.error(new Error(param));
}
RendImpl.prototype.rendFunctions["file"]=function(context, param) {
  return "";
}

RendImpl.prototype.rendFunctions["text"]=function(context, param) {
  return formatSpan(this.encode(param.text),param.style);
}
RendImpl.prototype.rendFunctions["title"]=function(context, param) {
  return drawParagraf(param.text,"T"+param.level);
}

RendImpl.prototype.rendFunctions["paragraph"]=function(context, param) {
  return drawParagraf(param);
}
RendImpl.prototype.rendFunctions["hline"]=function(context, param) {
  if(param=="double") {
    return "<w:p w:rsidR=\"006964DB\" w:rsidRDefault=\"006964DB\" w:rsidP=\"006964DB\" ><w:pPr><w:pBdr><w:top w:val=\"double\" w:sz=\"4\" w:space=\"1\" w:color=\"auto\"/></w:pBdr></w:pPr></w:p>";
  }
  return "<w:p w:rsidR=\"006964DB\" w:rsidRDefault=\"006964DB\" w:rsidP=\"006964DB\" ><w:pPr><w:pBdr><w:top w:val=\"single\" w:sz=\"4\" w:space=\"1\" w:color=\"auto\"/></w:pBdr></w:pPr></w:p>";
}
RendImpl.prototype.rendFunctions["list"]=function(context, param) {
  ret="";
	var drawItem=function(item) {
		var t="28";
		if(item.style=="*")
			t="30";
		else if(item.style=="-")
      t="29";
    var left=142+((Number(item.level)-1) * 284)
		var style="<w:pPr><w:pStyle w:val=\"Corpotesto\"/><w:numPr><w:ilvl w:val=\""+(Number(item.level)-1)+"\"/><w:numId w:val=\""+t+"\"/></w:numPr><w:ind w:left=\""+left+"\" w:hanging=\"142\"/></w:pPr>";
		if(t=="7")
			style+=formatSpan(" ");
		ret+=drawParagraf(style+item.text);
	};
	if(param!=null && param.length>0) {
    param.forEach(drawItem);
	}
	return ret;
}
RendImpl.prototype.rendFunctions["anchor"]=function(context, param) {
  var ctx=this.getDocxContext(context);
  ctx.refId++;
	return "<w:bookmarkStart w:id=\""+ctx.refId+"\" w:name=\""+param.id+"\"/>"+param.text+"<w:bookmarkEnd w:id=\""+ctx.refId+"\"/>";
}
RendImpl.prototype.rendFunctions["sourceinline"]=function(context, param) {
  return formatSpan(this.encode(param),"inline");
}
RendImpl.prototype.rendFunctions["source"]=function(context, param) {
  var ret="";
  var idx=0;
  var prefix="";
  utl.splitFromRegEx(param.text,[ /<w:t[^>]+>[^<]*[\n][^<]*<\/w:t><\/w:r>/gm ], function(v,id){
    var pos=v[0].indexOf("\n");
    while(pos>=0) {
      var txt=prefix+param.text.substring(idx,v.index+pos);
      prefix="";
      if(txt.indexOf("<w:t")==-1)
        txt=`<w:r><w:t xml:space="preserve" >`+txt;
      ret+=drawParagraf(txt+"</w:t></w:r>","source");
      idx=v.index+pos+1;
      pos=v[0].indexOf("\n",pos+1);
    }
    prefix='<w:r><w:t xml:space="preserve" >';
    /*
    ret+=drawParagraf('<w:r><w:t xml:space="preserve" >'+v[0].substring(idx-v.index),"source");

    idx=v.index+v[0].length;
    */
  })
  if(idx<param.text.length) {
    ret+=drawParagraf(prefix+param.text.substring(idx),"source")
  }
  return ret;
}
RendImpl.prototype.rendFunctions["link"]=function(context, param) {
  var ctx=this.getDocxContext(context);
  if(/^#.+$/i.test(param.url)) {
		return "<w:hyperlink w:anchor=\""+param.url.substring(1)+"\" w:history=\"0\">"+param.text+"</w:hyperlink>";
	}
	ctx.refId++;
	var id="extId"+ctx.refId;
	ctx.refs.push({type: "link", val: param.url, id: id});
	return "<w:hyperlink r:id=\""+id+"\" w:history=\"0\">"+param.text+"</w:hyperlink>"
		+"<w:bookmarkStart w:id=\"0\" w:name=\"_GoBack\"/><w:bookmarkEnd w:id=\"0\"/>";
}
RendImpl.prototype.rendFunctions["img"]=function(context, param) {
  var ctx=this.getDocxContext(context);
  ctx.imageId++;
	var id="img"+ctx.imageId;
	var file=param.url;
	var ext="";
	if(path.isAbsolute(file)) {
		ext=path.extname(file);
		ctx.images.push({file: file, id: id})
	} else {
		ctx.images.push({url: param.url, id: id})
		ext=path.extname(param.url);
  }
  var info={width: 320, height: 200}
  if(fs.existsSync(file)) {
    var data=fs.readFileSync(file);
    info = imageinfo(data);
  } 

	return "<w:r><w:drawing><wp:inline distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\"><wp:extent cx=\"1957782\" cy=\"1094812\"/>"
						+"<wp:effectExtent l=\"0\" t=\"0\" r=\"4445\" b=\"0\"/>"
						+"<wp:docPr id=\""+ctx.imageId+"\" name=\"Immagine "+ctx.imageId+"\"/>"
						+"<wp:cNvGraphicFramePr>"
							+"<a:graphicFrameLocks xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" noChangeAspect=\"1\"/>"
						+"</wp:cNvGraphicFramePr>"
						+"<a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">"
							+"<a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
								+"<pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
									+"<pic:nvPicPr>"
										+"<pic:cNvPr id=\"0\" name=\""+id+ext+"\"/>"
										+"<pic:cNvPicPr/>"
									+"</pic:nvPicPr>"
									+"<pic:blipFill>"
										+"<a:blip r:embed=\""+id+"\">"
											+"<a:extLst>"
												+"<a:ext uri=\"{28A0092B-C50C-407E-A947-70E740481C1C}\">"
													+"<a14:useLocalDpi xmlns:a14=\"http://schemas.microsoft.com/office/drawing/2010/main\" val=\"0\"/>"
												+"</a:ext>"
											+"</a:extLst>"
										+"</a:blip>"
										+"<a:stretch>"
											+"<a:fillRect/>"
										+"</a:stretch>"
									+"</pic:blipFill>"
									+"<pic:spPr>"
										 +"<a:xfrm>"
										 	+"<a:off x=\"0\" y=\"0\"/>"
										 	+"<a:ext cx=\""+info.width+"\" cy=\""+info.height+"\"/>"
										 +"</a:xfrm>"
										+"<a:prstGeom prst=\"rect\">"
											+"<a:avLst/>"
										+"</a:prstGeom>"
									+"</pic:spPr>"
								+"</pic:pic>"
							+"</a:graphicData>"
						+"</a:graphic>"
					+"</wp:inline>"
				+"</w:drawing>"
			+"</w:r>";
  
}
RendImpl.prototype.rendFunctions["pagebreak"]=function(context, param) {
  return "<w:p><w:r><w:br w:type=\"page\"/></w:r></w:p>";
}
RendImpl.prototype.rendFunctions["table"]=function(context, table) {
  var ret="<w:tbl><w:tblPr><w:tblStyle w:val=\"Grigliatabella\"/><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblLook w:val=\"04A0\" w:firstRow=\"1\" w:lastRow=\"0\" w:firstColumn=\"1\" w:lastColumn=\"0\" w:noHBand=\"0\" w:noVBand=\"1\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"3259\"/><w:gridCol w:w=\"3259\"/><w:gridCol w:w=\"3260\"/></w:tblGrid>";
  function drawRow(row,style) {
    var ret="<w:tr >";
    row.forEach((td)=>{
      ret+="<w:tc><w:tcPr><w:tcW w:w=\"6518\" w:type=\"dxa\"/>";
      if(td.dim>1)
        ret+=`<w:gridSpan w:val="${td.dim}"/>`
      ret+=`</w:tcPr>`
      ret+=td.text.replace(/^<w:p>/,`<w:p><w:pPr><w:pStyle w:val="${style+td.align.toLowerCase()}" /></w:pPr>`);
      ret+="</w:tc>"
    });
    ret+="</w:tr>";
    return ret;
  }

  //console.log(table);
  table.head.forEach((r)=>{
    ret+=drawRow(r,"thead");
  });
  table.body.forEach((r)=>{
    ret+=drawRow(r,"tbody");
  });

  ret+="</w:tbl>";
  return ret;
}

RendImpl.prototype.rendFunctions["quote"]=function(context, param) {
  var ret="";
  param.forEach((q)=>{
    ret+=drawParagraf(q.text,"QUOTE"+q.level);
  });
  return ret;
}


var htmlChars = {
	'¢' : 'cent',
  '¥' : 'yen',
  '€': 'euro',
  '©' :'copy',
  '<' : 'lt',
  '>' : 'gt',
  '"' : 'quot',
  '&' : 'amp',
  '\'' : '#39'
};
var reHtmlChars=new RegExp("["+Object.keys(htmlChars).join()+"\u00A0-\u9999]","gim");

RendImpl.prototype.encode=function(text) {
  return text.replace(reHtmlChars,function(c) {
    if(htmlChars[c])
		  return "&"+htmlChars[c]+";";
    return '&#'+c.charCodeAt(0)+';';
	});
}


module.exports = RendImpl;