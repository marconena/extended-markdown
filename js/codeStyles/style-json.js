const Style=require("./style.js");
function StylerMd() {
    Style.call(this);
}
StylerMd.prototype = Object.create(Style.prototype);
StylerMd.prototype.type="md";
/**
 * Supported style types:
 * styles:
 * bold
 * italic
 * underline
 * strikethrough
 * inline
 * code: 
 * 	oper
 * 	cmd
 * 	key
 * 	key1
 * 	key2
 * 	string
 * 	number
 * 	const
 * 	comment 
 */
StylerMd.prototype.conf=[
    
        {
            re: /("|')((\\\"|\\\'|.)+?)(\1)([ \n\t]*)(:)/gm,
            1: "oper",
            2: "cmd",
            3: null,
            4: "oper",
            5: "",
            6: "oper",

        },
        {
            re: /(('|")((\\\'|\\\"|.)+?)('|"))/gm,
            1: "string"
        },
        {
            re: /([0-9.]+)/gm,
            1: "number"
        },
        {
            re: /(true|false|null)/gm,
            1: "key"
        },
        {
            re: /(\{|\}|,)/gm,
            1: "oper"
        },
    ]
    

module.exports = StylerMd;