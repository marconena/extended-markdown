const Style=require("./style.js");
function StylerMd() {
    Style.call(this);
}
StylerMd.prototype = Object.create(Style.prototype);
StylerMd.prototype.type="md";
/**
 * Supported style types:
 * styles:
 * bold
 * italic
 * underline
 * strikethrough
 * inline
 * code: 
 * 	oper
 * 	cmd
 * 	key
 * 	key1
 * 	key2
 * 	string
 * 	number
 * 	const
 * 	comment 
 */
StylerMd.prototype.conf=[
    
        {
            re: /^[ ]+(```)(.*)($[\S\s]+?)^[ ]+(```)$/gm,
            1: "oper",
            2: "cmd",
            3: "string",
            4: "oper"
        },
        {
            re: /(`)((\\`|.)+)(`)$/gm,
            1: "oper",
            2: "string",
            3: null,
            4: "oper"
        },
        {
            re: /(<\{\{)([^{}:]+)(:(.*?))?(\}\})([\s\S]+?)(\{\{)(\2)(\}\}>)/gm,
            1: "oper",
            2: "cmd",
            3: null,
            4: function(val) { return val ? this.macro("oper",":")+this.macro("string",this.encode(val)): ""; },
            5: "oper",
            6: "comment",
            7: "oper",
            8: "cmd",
            9: "oper"
        },
        {
            re: /(\{\{)([^:{}]+)(:(.*?))?(\}\})/gm,
            1: "oper",
            2: "cmd",
            3: null,
            4: function(val) { return val ? this.macro("oper",":")+this.macro("string",this.encode(val)) : ""; },
            5: "oper"
        },
        {
            re: /^(---|===|\^\^\^)$/gm,
            1: "oper"
        },
        {
            re: /^([ \t]*[\-*.]#?)([ \t])/gm,
            1: "oper",
            2: ""
        },
        {
            re: /^([#]+|[>]+)/gm,
            1: function(val) {
                return this.macro("key"," "+val);
            },
        },
        { re: /^(\|[-|:]+\|)$/gm, 1: "bold" },
        { re: /(\*\*[\s\S]+?\*\*)/gm, 1: "bold" },
        { re: /(\/\/[\s\S]+?\/\/)/gm, 1: "italic" },
        { re: /(\_\_[\s\S]+?\_\_)/gm, 1: "underline" },
        { re: /(\-\-[\s\S]+?\-\-)/gm, 1: "strikethrough" },
        { re: /(\|:|:\||\|)/gm, 1: "oper" },
    
    ]
    

module.exports = StylerMd;