var utl=require("../util.js");
var mdUtil=require("../macros.js");

function Styler() {

}
Styler.prototype.type="undefined";

Styler.prototype.encode=function(str) {
    return mdUtil.encode(str);
}
/**
 * Create a macro string in well format
 * @param {string} params type
 * @param {string} inside Block inside a macro
 */
Styler.prototype.macro=function(params,inside) {
    if(!inside) return "";
    var ret="";
    var char="";
    if(inside) char="<"
    
    ret+=char+"{{t";
    if(params) ret+=":"+params;
    ret+="}}"
    if(inside) {
        ret+=mdUtil.encode(inside);
        ret+="{{t}}>";
    }
    return ret;
}
/**
 * Supported style types:
 * styles:
 * bold
 * italic
 * underline
 * strikethrough
 * inline
 * code: 
 * 	oper
 * 	cmd
 * 	key
 * 	key1
 * 	key2
 * 	string
 * 	number
 * 	const
 * 	comment 
 */
Styler.prototype.conf=null
Styler.prototype.toStyleConf=function(conf,str) {
    var regExprs=[];
    conf.forEach((o)=>{
        regExprs.push(o.re);
    });
    var ret="";
    var idx=0;
    utl.splitFromRegEx(str,regExprs,(v,id)=>{
        ret+=mdUtil.encode(str.substring(idx,v.index));
        for(var i=0;i<v.length;i++) {
            if(typeof(conf[id][i])=="string") {
                ret+=this.macro(conf[id][i],v[i]);
            } else if(Array.isArray(conf[id][i])) {
                ret+=this.toStyleConf(conf[id][i],v[i]);
            } else if(typeof(conf[id][i])=="function") {
                ret+=conf[id][i].call(this,v[i]);
            }
        }
        idx=v.index+v[0].length;
    });
    ret+=mdUtil.encode(str.substring(idx));
    return ret;
}

Styler.prototype.toStyle=function(str) {
    if(!this.conf)
        return str;
    return this.toStyleConf(this.conf,str);
}


module.exports = Styler;