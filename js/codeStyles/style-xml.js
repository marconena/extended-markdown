const Style=require("./style.js");
function StylerMd() {
    Style.call(this);
}
StylerMd.prototype = Object.create(Style.prototype);
StylerMd.prototype.type="md";
/**
 * Supported style types:
 * styles:
 * bold
 * italic
 * underline
 * strikethrough
 * inline
 * code: 
 * 	oper
 * 	cmd
 * 	key
 * 	key1
 * 	key2
 * 	string
 * 	number
 * 	const
 * 	comment 
 */
StylerMd.prototype.conf=[
    
        {
            re: /^(<\?.*?\?>)/gm,
            1: "comment"
        },
        {
            re: /^(<!--[\s\S]+-->)/gm,
            1: "comment"
        },
        {
            re: /(<\/?)([a-z][a-z:0-9_.]*)([\s\S]*?)(\/?>)/gmi,
            1: "oper",
            2: "cmd",
            3: [
                {
                    re: /([a-z][a-z0-9_:]*)[ \t]*([=])[ \t]*(['"])(.*?)(\3)/gmi,
                    1: "key",
                    2: "oper",
                    3: "oper",
                    4: "string",
                    5: "oper",
                }  
            ],
            4: "oper"
        }
    
    ]
    

module.exports = StylerMd;