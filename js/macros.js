const fs=require("fs");
const pUtil=require("./pathUtils.js");
const path=require("path");
const utl=require("./util.js")
const EventEmitter = require('events');
const ee = new EventEmitter();


var addDebugInfo=true;


/**
 * DefaultContext
 */

var defaultContext={
	type: "context",
	/** 
	 * search: Search path
	 */
	searchPaths: [pUtil.toAbsolute(__dirname+"/../libmd")],
	stack: [],
	stackSrc: [],
	global: {}
}


/**
 * Object for handlind a read line for line of text
 * @param {string} str content string
 * @param {context} context Context object
 */
function LineReader(str, context) {
	this.str=str.replace(/\n\r|\r\n/gm,"\n").replace(/\r/gm,"\n");;
	this.lines=[0];
	this.stack=null;
	if(isContext(context)) {
		this.stack=context.stackSrc;
		var srcStack={ row: 0 };
		if(context.stack.length>0) {
			srcStack.file=context.stack.slice(-1)[0];
		}
		this.stack.push(srcStack);
	}
	var pos=0;
	while((pos=this.str.indexOf("\n",pos))>=0) {
		pos++;
		this.lines.push(pos);
	}
	/**
	 * Current row
	 */
	this.current=0;
	
	/**
	 * Get current row and not move next
	 */
	this.get=function() {
		if(this.current>=0 && this.current<this.lines.length) {
			var limit=this.limit();
			if(limit) {
				var str=this.str.substring(limit.start,limit.end);
				str=str.replace(/[\n]$/,"");
				if(addDebugInfo)
					str=str.replace(/^\{\{_FILE_.*?\}\}/,"");
				return str;
			}
		}
		return null;
	}
	/**
	 * get current row and move to next
	 */
	this.next=function() {
		if(this.current<this.lines.length) {
			var ret=this.get();
			if(this.stack) this.stack.slice(-1)[0].row=this.current+1;
			this.current++;
			
			return ret;
		}
		return null;
	}

	/**
	 * Move to prev row and get it
	 */
	this.prev=function() {
		if(this.current>0) {
			this.current--
			if(this.stack)
				this.stack.slice(-1)[0].row=this.current+1;
			return this.get();
		}
		return null;
	}
	/**
	 * Get current line start and end position, if is after las return next char
	 */
	this.limit=function() {
		if(this.current>=0 && this.current<this.lines.length) {
			return {start: this.lines[this.current], end: this.lines[this.current+1] ? this.lines[this.current+1]: this.str.length};
		}
		if(this.current>=this.lines.length)
			return {start: this.str.length+1};
		return null;
	}
	this.end=function() {
		if(this.stack)
			this.stack.pop();
	}
	this.getPos=function(col) {
		if(col==null) col=0;

	}

}
/**
 * Find a file to search on context search path
 * @param {string} file 
 * @param {context} context 
 */
function findFile(file,context) {
	if(isContext(context)) {
		if(context.stack.length>0) {
			var f=pUtil.findFileFromParent(file,context.stack.slice(-1)[0]);
			if(f)
				return f;
		}
		return pUtil.findFile(file,context.searchPaths);
	}
	return file;
}
/**
 * Varify a object is context type
 * @param {object} context 
 */
function isContext(context) {
	return context && context.type == "context";
}
/**
 * Trasform a object to default context object
 * @param {object} context 
 */
function toContext(context) {
	if(!isContext(context)) {
		var con=JSON.parse(JSON.stringify(defaultContext));
		if(context && typeof(context)=="object") {
			for(var n in con) {
				context[n]=con[n];
			}
		} else {
			context=con;
		}
	}
	return context;
}

/**
 * Convert a file to macro syntax
 * @param {string} file File path
 * @param {context} context Context object
 * @param {boolean} isMain If is true a file is a main 
 */
function fileToMacro(file,context, isMain) {
	context=toContext(context);
	file = findFile(file,context);
	if(file) {
		var str=fs.readFileSync(file).toString();
		context.stack.push(file);
		if(isMain) context.global["__MAIN_FILE__"]=file;
		var ret=toMacro(str,context, isMain)
		context.stack.pop();
		return ret;

	}
	return null;
}

/**
 * Function to encode a string (use \ for killer char)
 * @param {string} str String to encode
 */
function encodeStr(str) {
	var chars={
		"\n": "\\n",
		"\r": "\\r",
		"\t": "\\t"
	}
	if(str) {

		return str.replace(/\\|[{}\n\t\r<>]/g, function(str){
			return chars[str] ? chars[str] : "\\"+str;
		});
	}
	return str;
}

/**
 * Function to decode string
 * @param {string} str String to decode
 */
function decodeStr(str) {
	var chars={
		"n": "\n",
		"r": "\r",
		"t": "\t"
	}
	if(str) {
		return str.replace(/\\(.)/g, function(str,c){
			return chars[c] ? chars[c] : c;
		});
	}
	return str;
}



/**
 * Convert a markdown/macros to a onli macros string and context
 * @param {string} str String markdown/macros to convert to only macros
 * @param {context} context Context object
 * @param {boolean} isMain Is main file (if tru add a context global macro at start)
 * @returns {string} Macro sintax converted
 */
function toMacro(str,context, isMain) {
	context=toContext(context);
	var lines=new LineReader(str,context);
	str=mdDef.processMD(lines,context);
	lines.end();
	if(isMain ) {
		str=macro("globals",null,encodeStr(JSON.stringify(context.global,null,"  ")))+"\n"+str;
	}
	return str;
}

/**
 * Create a macro string in well format
 * @param {string} name Name of macro
 * @param {string} params Macro parameter string
 * @param {string} inside Block inside a macro
 */
function macro(name,params,inside) {
	var ret="";
	var char="";
	if(inside) char="<"

	ret+=char+"{{"+name;
	if(params) ret+=":"+params;
	ret+="}}"
	if(inside) {
		ret+=inside;
		ret+="{{"+name+"}}>";
	}
	return ret;
}

/**
 * get a macro error string
 * @param {string} message Error message
 * @param {context} context Macro context
 */
function macroError(message,context) {
	var stack="";
	if(context.stackSrc.length>0) {
		for(var i=context.stackSrc.length-1;i>=0;i--) {
			var s=context.stackSrc[i];
			if(stack) stack+="\n";
			if(s.file) stack+=s.file;
			stack+=":"+s.row;
		}
	}
	console.error(message,stack);
	return "\n"+macro("error",message,stack)+"\n";
}

function debugInfo(str,context) {
	if(addDebugInfo && isContext(context)) {
		if(context.stackSrc.length>0) {
			var param="";
			var s=context.stackSrc.slice(-1)[0];
			if(s.file)
				param+=s.file;
			param+=":"+s.row;
			return macro("_FILE_",param)+str;
		}
		
	}
	return str;
}

/**
 * Add a 
 * @param {string} code Code da append to style
 * @param {string} type Code tipe (md, xml, emd, ecc)
 */
function toCodeStyle(code,type) {
	/*code=code.replace(/[{}\\]/gm,function(s) {
		return "\\"+s;
	})*/
	if(type) {

		var file=path.resolve(__dirname,"./codeStyles/style-"+type+".js");
		if(fs.existsSync(file)) {
			try {
				var Styler=require(file);
				
				var ret= new Styler().toStyle(code);
				return mdDef.toPlainStyles(ret);
			} catch(e) {
				var msg="Style type \""+type+"\" not implemented ! file="+file+" error: "+e.message;
				ee.emit("error",new Error(msg));
			}
		}
	}
	return mdDef.toPlainStyles(encodeStr(code));
}


var mdDef = {
	processMD: function(lines,context) {
		var post=[];
		var ret="";
		var line=null;
		while((line=lines.next())!=null) {

			var def=this.getDef(line);
			if(def) {
				lines.prev();
				var val=def.fn.call(this,lines,context,def);
				if(val && typeof(val)=="string")
					ret+=val;
				else if(typeof(val)=="object") {
					post.push(val);
				}
			}
		}

		return ret;
	},
	/**
	 * Convert a str styling to plain style with multistyle
	 */
	toPlainStyles: function(str) {
		var stack=null;
		var style={lev: [], style:[]};
		var idx=0;
		var ret="";
		var text="";
		var toText=function(text,styles) {
			if(text) {
				if(styles.length >1) {
					styles.sort();

				}

				var ret="<{{t";
				ret+=styles.length >0 ? ":"+styles.join(";") : "";
				ret+="}}";
				ret+=text;
				ret+="{{t}}>";
				return ret;
				
			}
			return "";
		}
		utl.splitFromRegEx(str,[
			/\\\\|\\<|(<)?\{\{([^:{}]+?)(:((\\\}|.)+?))?\}\}(>)?/gm
			], (v,id)=>{
				if(v[2]) {
					if(stack) {
						if(stack.name==v[2]) {
							// Searching a end of block
							if(v[1]=="<") {
								stack.lev++;
							} else if(v[6]) {
								stack.lev--;
							}
							if(stack.lev==0) {
								stack=null;
								ret+=str.substring(idx,v.index+v[0].length);
								text="";
								idx=v.index+v[0].length;
							}
						}
					} else if(v[2]=="t") {
						text+=str.substring(idx,v.index);
						if(v[1]=="<") {
							// Start text
							if(v[4]) {
								// have a style
								var s=v[4].split(";");
								var ss=[];
								s.forEach((sty)=> {
									if(sty&&style.style.indexOf(sty)<0) {
										ss.push(sty);
									}
								});
								if(ss.length>0) {
									ret+=toText(text,style.style);
									text="";
									style.lev.push(style.style.length);
									style.style=style.style.concat(ss);
									
								}
							}
							
						} else if(v[6]) {
							if(style.lev.length>0) {
								// end text
								var lev=style.lev.pop();
								ret+=toText(text,style.style);
								text="";
								style.style.splice(lev,style.style.length-lev);
							}
							
						}
						idx=v.index+v[0].length;
					} else {
						text+=str.substring(idx,v.index);
						ret+=toText(text,style.style);
						text="";
						if(v[1]) {
							stack={lev: 1, name: v[2]};
							idx=v.index;
						} else {
							ret+=v[0];
							idx=v.index+v[0].length;
						}
					}
				}
			});

			ret+=toText(text+str.substring(idx),style.style);
		return ret;
	},
	getDef: function(line) {
		for(var i=0;i<this.defs.length;i++) {
			if(this.defs[i].re.test(line)) {
				return this.defs[i];
			}
		}
		return null;
	},
	processStyles: function(str, context, insideText) {
		var ret="";

		var style=null;
		while((style=this.findStyle(str))) {
			if(style.val.index>0) {
				if(insideText)
					ret+=str.substring(0,style.val.index);
				else 
					ret+=macro("t",null,str.substring(0,style.val.index));
			}
			var value=style.def.fn.call(this,style.val,context);
			if(value) {
				ret+=value;
				
			}
			str=str.substring(style.val.index+style.val[0].length);
		}
		if(str) {
			if(insideText)
				ret+=str;
			else
				ret+=macro("t",null,str);

		}
		if(!insideText)
			return macro("styleblock",null,this.toPlainStyles(ret));
		return this.toPlainStyles(ret);
	},
	findStyle: function(str) {
		var ret=null;
		for(var i=0;i<this.defStyles.length;i++) {
			var style=this.defStyles[i];
			var v=style.re.exec(str);
			if(v && (!ret || ret.val.index > v.index)) {
				ret={val: v, def: style};
			}
		}
		return ret;

	},
	defMacros: {
		/**
		 * Include pre defined macro
		 */
		"include": function(param,inside,context) {
			var file=findFile(param,context);
			if(file) {
				if(context.stack.indexOf(file)>=0)
					return macroError("Included file \""+param+"\" is recursive included ! ",context);

				var ret=fileToMacro(file,context);
				return ret;
			} else {
				return macroError("Included file \""+param+"\" not finded !" ,context);
			}
		}
	},
	defStyles: [
		{
			type: "img",
			re: new RegExp("\\!\\[(.+?)\\]\\\(([^\"]+)\\\)","m"),
			fn: function(v,context) {
				var file=findFile(v[2],context);
				if( file )
					return macro("img",file,this.processStyles(v[1],context));
				else if(/^https?:.*$/i.test(v[2]))
					return macro("img",v[2],this.processStyles(v[1],context));
				return macro("img",v[2],this.processStyles(v[1],context));
			}
		},
		{
			type: "id",
			re: new RegExp("\\[(.+?)\\]\\\"([^\"]+)\\\"","m"),
			fn: function(v,context) {
				return macro("a",v[2],this.processStyles(v[1],context));
			}
		},
		{
			type: "link",
			re: new RegExp("\\[(.+?)\\]\\(([^)]+)\\)","m"),
			fn: function(v,context) {
				return macro("link",v[2],this.processStyles(v[1],context));
			}
		},
		{
			type: "bold",
			re: new RegExp("\\*\\*([\\s\\S]+?)\\*\\*","m"),
			fn: function(v,context) {
				return macro("t","bold",this.processStyles(v[1],context, true));
			}
		},
		{
			type: "italic",
			re: /\/\/([\s\S]+?)\/\//m,
			fn: function(v,context) {
				return macro("t","italic",this.processStyles(v[1],context, true));
			}
		},
		{
			type: "underline",
			re: new RegExp("\\_\\_([\\s\\S]+?)\\_\\_","m"),
			fn: function(v,context) {
				return macro("t","underline",this.processStyles(v[1],context, true));
			}
		},
		{
			type: "strikethrough",
			re: new RegExp("\\-\\-([\\s\\S]+?)\\-\\-","m"),
			fn: function(v,context) {
				return macro("t","strikethrough",this.processStyles(v[1],context, true));
			}
		},
		{
			type: "inlinesource",
			re: new RegExp("`((\\\\\\\\|\\\\`|[^`])*)`","m"),
			fn: function(v,context) {
				var str=v[1].replace(/\\(\\|`)/gm,function(str,c) {
					return c;
				})
				return macro("inlinesrc",null,str);
			}
		},
		{
			type: "styleMacro",
			re: /\<\{\{([^:{}]+?)(:(\\\}|.)*?)?\}\}[\s\S]+\{\{\1\}\}\>|\{\{(\\\}|.)*?\}\}?/m,
			fn: function(v,context) {
				return v[0];
			}
		}
	],
	defs: [
		{
			type: "code",
			re: new RegExp("^```([a-z0-9]+)?$"),
			reEnd: new RegExp("^```$"),
			fn: function(lines,context,me) {
				var start=lines.current;
				var v=me.re.exec(lines.next());
				var code="";
				var line=null;
				while((line=lines.next())!=null) {
					if(me.reEnd.test(line)) {
						break;
					}
					if(code) code+="\n";
					code+=line;
				}
				if(line!=null)
					return macro("code",v[1],toCodeStyle(code+"\n",v[1]));
				return macroError("Code start at row "+start+" not closed ",context);
			}
		
		},
		{
			type: "block",
			re: new RegExp("^[ \t]*(<{\\{([^:]+)([:]((\\\\\\}|.)+?))?\\}\\})(.*)"),
			fn: function(lines,context,me) {
				var v=me.re.exec(lines.next());
				var start=v.index;
				var name=v[2];
				var re=new RegExp("(\\<)?\\{\\{"+v[2]+"([:](\\\\\\}|.)+?)?\\}\\}(\\>)?");
				var level=1;
				var retStart=debugInfo(v[1],context)
				var ret="";
				var line=v[6] ? v[6] : lines.next();
				var newline=!v[6];
				while(line!=null) {

					var vv=null;
					while((vv=re.exec(line))) {
						if(vv[1])
							level ++;
						else 
							level--;
						if(newline) ret+="\n";
						newline=false;
						ret+=line.substring(0,vv.index+vv[0].length);
						if(level==0)
							break;
						line=line.substring(vv.index+vv[0].length);
					}
					if(vv) {
						break;
					} else {
						if(newline) ret+="\n";
						newline=false;
						ret += line;
					}
					line=lines.next();
					newline=true;
				}
				if(typeof(this.defMacros[name])=="function") {
					return this.defMacros[name].call(this,v[4],ret.substring(0,ret.length-(name.length+5)),context);
				}

				return retStart+ret+"\n";
			}
		
		},
		{
			type: "macro",
			re: new RegExp("^[ \t]*(\\{\\{(.+?)([:]((\\\\\\}|.)+?))?\\}\\})[ \t]*$"),
			fn: function(lines,context,me) {
				var v=me.re.exec(lines.next());
				var name=v[2];
				if(typeof(this.defMacros[name])=="function") {
					return this.defMacros[name].call(this,v[4],null,context);
				}
				return debugInfo(v[1],context)+"\n";
			}
		
		},
		{
			type: "hline",
			re: new RegExp("^([-][-][-]+)|[=][=][=]+$"),
			fn: function(lines,context,me) {
				var v=me.re.exec(lines.next());
				return debugInfo(macro("hline",v[1]?"single":"double")+"\n",context);
			}
		
		},
		{
			type: "pagebreak",
			re: new RegExp("^\\^\\^\\^+$"),
			fn: function(lines,context,me) {
				var v=me.re.exec(lines.next());
				return debugInfo(macro("pbreak")+"\n");
			}
			
		},
		{
			type: "title",
			re: new RegExp("^(#+)[ \t]+(.*)$"),
			fn: function(lines,context,me) {
				var v=me.re.exec(lines.next());
				var level=v[1].length;
				var text=v[2].trim();
				var punt=null;
				if(text.substr(0,1)!='-') {

					if(!context.titles) {
						context.titles={
							id: 0,
							levels: [],
							titles: []
						};
					}
					var id="T"+(context.titles.id++);
					while(context.titles.levels.length<level) {
						context.titles.levels.push(0);
					}
					if(context.titles.levels.length>level) {
						context.titles.levels.splice(level,context.titles.levels.length-level);
					}
					context.titles.levels[level-1]++;
					
					var inner="";
					if(text.substr(0,1)=='#') {
						text=text.substring(1);
						punt="";
						for(var i=0;i<level;i++) {
							if(i>0) punt+=".";
							punt+=String(context.titles.levels[i])
						}
						inner=macro("t","bold",punt)+this.processStyles(text,context);
					} else {
						inner=this.processStyles(text,context);
					}
					if(!context.global.toc) {
						context.global.toc=[];
					}
					context.global.toc.push({id: id, level: level, text: inner.replace(/[<]?\\{\\{a([:](\\\}|.)*?)?\\}\\}[>]?/gm, "")});
					return debugInfo(macro("title",level,macro("a",id,inner))+"\n",context);
				} else {
					text=text.substr(1).trim();
					return debugInfo(macro("title",level,this.processStyles(text,context))+"\n",context);
				}
				
			}
		},
		{
			type: "list",
			re: new RegExp("^([ \t]*)([\\-]|[*]|\\.)(#)?[ \t]+(.*)$"),
			fn: function(lines,context,me) {
				var line=null;
				var li="";
				var levels=[];
				while((line=lines.next())!=null) {
					if(me.re.test(line)) {
						var v=me.re.exec(line);
						var level=Number(Number(v[1].replace(/\t/g,"  ").length/2).toFixed(0));
						while(levels.length<=level)
							levels.push(0);
						if(levels.length>(level+1)) {
							levels.splice(level+1,levels.length-level-1);
						}
						levels[level]++;
						if(v[3]=="#") {
							var num="";
							levels.forEach((lev)=>{
								if(num) num+=".";
								num+=lev;
							})

							li+=debugInfo(macro("li",(level+1),macro("t","bold",num+" ")+this.processStyles(v[4],context))+"\n",context);
						} else {
							li+=debugInfo(macro("li",(level+1)+","+v[2],this.processStyles(v[4],context))+"\n",context);
						}

					} else {
						lines.prev();
						break;
					}
				}
				return macro("ul",null,"\n"+li)+"\n";
			}
		},
		{
			type: "quote",
			re: new RegExp("^([>]+)[ \t]+(.*)$"),
			fn: function(lines,context,me) {
				var line=null;
				var text="";
				var levels=[];
				while((line=lines.next())!=null) {
					if(me.re.test(line)) {
						var v=me.re.exec(line);
						var level=v[1].length;
						text+=debugInfo(macro("quote",level,this.processStyles(v[2],context))+"\n",context);
					} else {
						lines.prev();
						break;
					}
				}
				return macro("quoteblock",null,"\n"+text)+"\n";
			}
		},
		{
			type: "table",
			re: new RegExp("^[|](.*)$"),
			fn: function(lines,context,me) {
				var lim=lines.limit();
				var reLine=/^[|](.*)([\\|])$/;
				var reConf=/^[|]([:]?[-]+[:]?[|])+$/
				var reEndLine=/^.*[|]$/
				var lim=lines.limit();
				var line=null;
				var inside="";
				var align="";
				var tdType="rowHead"


				var makeRow=function(str,type,context) {
					var ar=str.split(/[|]/gm);
					for(var i=0;i<ar.length;i++) {
						var s;
						while(ar[i+1] && (s=ar[i]) && s.charAt(s.length-1)=="\\") {
							ar.splice(i,2,s.substr(0,s.length-1)+"|"+ar[i+1]);
						}
					}
					var td=[];
					for(var i=0;i<ar.length;i++) {
						if(ar[i]=="-") {
							if(td.length>0)
								td[td.length-1].colspan++;
							
						} else {
							var v=/^(\:)?([\s\S]*?)(\:)?$/.exec(ar[i]);
							var o={colspan: 1, inside: toMacro(v[2],context)};
							if(v[1] || v[3]) 
								o.align=(v[1]==v[3] ? "C":(v[1]==":" ? "L" : "R"))
							else 
								o.align="*"
							td.push(o);
						}
					}
					var ret="\n";
					for(var i=0;i<td.length;i++) {
						ret+=macro("td",td[i].align+td[i].colspan,"\n\t"+td[i].inside)+"\n";
					}
					return macro(type,null,ret)+"\n";

				}
				while((line=lines.next())) {
					if(reConf.test(line)) {
						tdType="rowBody";
						var l=line.substr(1,line.length-2).split("|");
						l.forEach((str)=>{
							var v1=/([:])?[-]+([:])?/.exec(str);
							align+= (v1[1]==v1[2] ? "C" : (v1[1] ? "L": "R"));
						});
					} else {
						var v=reLine.exec(line);
						if(v) {
							if(v[2]=="|") {
								inside+=debugInfo(makeRow(line.substring(1,line.length-1),tdType,context),context);
							} else {
								var totLine=line.substr(1,1)+debugInfo(line.substring(2,line.length-1)+"\n",context);
								while((line=lines.next())) {
									lim.end=lines.limit().start-1;
									var c=line.charAt(line.length-1);
									if(c=="\\") {
										totLine+=debugInfo(line.substring(0,line.length-1)+"\n",context);
									} else if(c=="|") {
										totLine+=debugInfo(line.substring(0,line.length-2),context)+line.substr(line.length-2,1);
										inside+=makeRow(totLine,tdType,context);
										break;
									} else {
										return macroError("Table row not ended !",context);
									}	
								}

							}
						} else {
							lines.prev();
							break;
						}
					}
				}
				return macro("table",align,"\n"+inside)+"\n";
				
			}
		},
		{
			type: "paragtaph",
			re: new RegExp("^.*\\S.*$"),
			fn: function(lines,context,me) {
				var line=null;
				var text="";
				while((line=lines.next())!=null) {
					var def=this.getDef(line);
					if(!me.re.test(line) || !def || def.type!="paragtaph") {
						lines.prev();
						break;
					} else if(line.charAt(line.length-1)==".") {
						if(text)text+=" ";
						text+=debugInfo(line,context);
						break;
					}
					if(text)text+=" ";
					text+=debugInfo(line,context);
				}
				return macro("p",null,this.processStyles(text,context))+"\n";
			}
			
		}
	]
};

function getRenderObject(type) {
	var file="./renders/rend"+( type ? "-"+type: "")+".js";
	try {
		var Render=require(file);
		return new Render(defaultContext.searchPaths);
	} catch(e) {
		var msg="Render type \""+type+"\" not implemented ! file="+file+" error: "+e.message;
		console.error(msg);
		ee.emit("error",new Error(msg));
	}
	return null;
}

module.exports = {
	/**
	 * Process style inside a stiles
	 */
	processStyles: function(str) {
		var context=toContext(null);
		return mdDef.processStyles(str,context,true);
	},
	/**
	 * Function to convert a markdown to macro sintax
	 */
	fileToMacro: function(file,context) {
		var ret=fileToMacro(file,null,true);
		return ret;
	},
	/**
	 * Convert a string to macro syntax
	 */
	stringToMacro: function(file,context) {
		var ret=toMacro(file,null,true);
		return ret;
	},
	/**
	 * Render a macro syntax to a file
	 * @param {string} macro String on format macros
	 * @param {string} out Output file
	 * @param {object} globals globals definitions
	 * @param {string} template Template output to use
	 * @param {function} fn Function called at end of render file
	 * @return {boolean} true if created false if not created
	 */
	renderFile: function(macro,out,globals, template, fn) {
		var type=path.extname(out);
		if(type) type=type.substr(1);
		
		var rend= getRenderObject(type);
		if(rend) {
			rend.globals=globals;
			return rend.renderFile(macro,out,template, fn);
		}
		return null;
	},
	/**
	 * Render a macro syntax to a file
	 * @param {string} macro String on format macros
	 * @param {string} type render type if omit take a output extension
	 * @return {object} return object rendered
	 */
	render: function(macro,type) {
		var rend= getRenderObject(type);
		if(rend)
			return rend.render(macro);
		return null;
	},
	defaultContext: defaultContext,
	encode: encodeStr,
	decode: decodeStr
}
