var util=require("./utils.js");
var fs = require('fs');
console.log("START...");

var inName="original";
var outName="../template";


var confTemplate = {
		"docx": {
			content: "/word/document.xml",
			re: new RegExp("<w:p.*?{{BODY}}.*?</w:p>","gm"),
			body: "{{{BODY}}}"
		},
		"odt": {
			content: "/content.xml",
			re: new RegExp("<text:p.*?{{BODY}}.*?</text:p>","gm"),
			body: "{{{BODY}}}"
		}
	}

function createTemplate(fileIn, fileOut ) {
	
	
	
	var ext=new RegExp("\\.([^.]+)$").exec(fileIn)[1];
	console.info("Pack template: "+fileIn+" to "+fileOut+" type="+ext);
	
	var conf=confTemplate[ext];
	if(conf) {
		var outDir="out-"+ext;
		util.removeDir(outDir);
		fs.mkdirSync(outDir);
		util.unzipTo(fileIn,outDir);

		var fileContent=conf["content"];

		fs.writeFileSync("content.xml",fs.readFileSync(outDir+fileContent));



		var content=fs.readFileSync(outDir+fileContent).toString();
		//~ console.log("CONTENT:"+content);
		content=content.replace(conf.re,function(txt){
			return "{{{BODY}}}";
			
		});
		
		fs.writeFileSync(outDir+fileContent,content);

		util.zipDirectoryTo(outDir,fileOut, function() {
			util.removeDir(outDir);
		});
	} else {
		console.error("Not configure template for type: "+ext);
	}

}

for(var type in confTemplate) {
	console.log(type);
	createTemplate(inName+"."+type,outName+"."+type)
}

//~ util.removeDir("out");

console.log("...END");