var fs = require('fs');
var AdmZip = require('adm-zip');
const Path = require('path');
var JSZip = require("jszip");



var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

var getAllFiles = function(path) {
  var ret=[];
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        var dirRet=getAllFiles(curPath);
        dirRet.forEach(function(file) {
          ret.push(file);
        });
      } else { // delete file
        ret.push(curPath);
      }
    });
  }
  return ret;
};


function zipDirectoryToWrong(dir,fileOut) {
  var files=getAllFiles(dir);
  var zipWriter = new AdmZip();
	files.forEach((f)=>{
		var fileLocalPath=f.substring(dir.length+1);
		zipWriter.addFile(fileLocalPath,fs.readFileSync(f),'', 0644 << 16);
	});
	zipWriter.writeZip(fileOut);
}

function zipDirectoryTo(dir,fileOut, fnFinish) {
  var files=getAllFiles(dir);
  var zipWriter = new JSZip();
  var dirs={"": zipWriter};
  files.forEach((f)=>{
		var fileLocalPath=f.substring(dir.length+1);
        var localDir="";
        if(fileLocalPath.indexOf("/")>0) {
          localDir=fileLocalPath.substring(0,fileLocalPath.lastIndexOf("/"));
          fileLocalPath=fileLocalPath.substring(fileLocalPath.lastIndexOf("/")+1);
          dirs[localDir]=zipWriter.folder(localDir);
        }
        console.log(f);
         dirs[localDir].file(fileLocalPath,fs.readFileSync(f));
	});
	zipWriter.generateNodeStream({type:'nodebuffer',streamFiles:true}).pipe(fs.createWriteStream(fileOut)).on('finish', function () {
                console.log(fileOut+" written.");
                if(typeof(fnFinish)=="function")
                  fnFinish();
            });
}


function unzipTo(file,dir) {
  var zip = new AdmZip(file);
	zip.extractAllTo(dir,true);
}


function mkdir(path) {
  var dir=Path.normalize(path);
  if(!fs.existsSync(dir)) {
    var parent=Path.dirname(dir);
    if(!fs.existsSync(parent))
      mkdir(parent);
    fs.mkdirSync(path);
  }
}

module.exports = {
  removeDir: deleteFolderRecursive,
  getAllFiles: getAllFiles,
  zipDirectoryTo:  zipDirectoryTo,
  unzipTo: unzipTo,
  mkdir: mkdir
}
