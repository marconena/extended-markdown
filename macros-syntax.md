# # Macro syntax

There is two type of macro a command macro and a block macro

## # Command macro

Syntax: `{{name[:param]}}`
- **name**: name of macro
- **param**: string param

### # Defined command macro

- `{{hline:<type>}}`: Horizontal line, type is `single` or `double`
- `{{pbreak}}`: Page break
- `{{include:<file>}}`: Include a file, file search on current file path or on configured context searchPaths (default is on extended markdown directori + libmd)
- `{{now:format}}`: Draw current date: format is (default is `Y/M/D h:m:s` example `{{now:Y-M-D}}`=** {{now:Y-M-D}}**):
  * **Y**: year 4 chars (ex 2017)
  * **y**: year 2 chars (ex 17)
  * **M**: month 2 chars (ex 09)
  * **D**: month day 2 chars (ex 28)
  * **h**: hour 2 chars (ex 18)
  * **m**: Minutes 2 chars (ex 22)
  * **s**: Seconds 2 chars (ex 31)
  * **ms**: Milli seconds 3 chars (ex 035)
- `{{toc}}`: Insert a table of context


## # Blok macro

Syntax: `<{{name[:param]}}body{{<name>}}>`
- **cmd**: name of macro
- **param**: string param 
- **body**: inside of macro

### # Defined block macros

- `<{{#p}}insideMacros{{/p}}>`: output a paragraph, style is a paragraph style
- `<{{#t:style}}text{{/t}}>`: output a text inside a paraghraph, style is a span style
- `<{{#title:level}}insideMacros{{/title}}>`: Title (level=Title level)
- `<{{#a:id}}insideMacros{{/a}}>`: anchor (id=anchor identifier)
- `<{{link:url}}insideMacros{{/link}}>`: Link, if id url start with a # is a link to internal anchor ( example `#<id>` for internal ancor, 'https://www.google.com' example of external link)
- `<{{inlinesrc}}text{{inlinesrc}}>`: source in line
- `<{{code:type}}text{{code}}>`: multiline source code. (type is type of source , xml, json, ecc
- `<{{quoteblock}}<{{quote:level}}insideMacros{{quote}}>{{quoteblock}}>`: quote element (quote element is repeated) level=level o quote 
- `<{{ul}}<{{li:level,type}}insideMacros{{li}}>{{ul}}>`: Draw a list (li is repeted and have this attributes; level=li level, type=* circle, - square, . none).
- `<{{img:url}}inside macros{{img}}>`: Draw a image.
- `<{{table:defaultAlign}}<{{rowHead}}<{{td:alignAndColspan}}insideMacros{{td}}>{{rowHead}}><{{rowBody}}<{{td:alignAndColspan}}insideMacros{{td}}>{{rowBody}}>{{table}}>`: Draw a table (defaultAlign=default cell align chars L C or R, alignAndColspan= First char L C R or * for default cell align and rest is a column span value )
- `<{{var:name}}value{{var}}>`: define a variable.
- `{{out:name,type}}`: draw an variable type is a variable inner type: 
  - **md**=Markdown expression (default)
  - **macro**=macro syntax.

**__Examples:__**.
**__Variable definitions__**.
```md
<{{var:var01}}My name is **MARCO**{{var}}>
<{{var:var02}}
this is a __multiline__ markdown.
list:
- A
-# B
  * C
  *# D
    . E
    .# F
{{var}}>
Output: {{out:var01,md}}.
Output: {{out:var01,md}}.
{{out:var02}}
<{{var:var03}}{{now}}{{var}}>
data=** {{out:var03}}**
```

<{{var:var01}}My name is **MARCO**{{var}}>
<{{var:var02}}
this is a __multiline__ markdown.
list:
- A
-# B
  * C
  *# D
    . E
    .# F
{{var}}>
Output: {{out:var01,md}}.
Output: {{out:var01,md}}.
{{out:var02}}
<{{var:var03}}{{now}}{{var}}>
data=** {{out:var03}}**

### # Extende macro syntax with javascript

Is possible extend macro syntax with adding a javascript. For make this must use a `extend` macro.
```md
<{{extend:datetime}}
def = {
  fn: function(param,inside,context) {
    
    return "**"+new Date()+"**";
  },
  type: "md"
}
{{extend}}>
Current date: {{datetime}}
```
Syntax is `<{{extend:macro Name}}definition{{extend}}>`
- **macro Name**: name of macro
- **definition**: javascript code with setting a def structure this structure have this properties.
  - __fn__ Mandatory: function defined with parameters:
    - param: parameter added on cal function `{{<Macro Name>:param}}`
    - inside: inside text if is called with a block `{{<Macro Name>:param}}< inside >{{/<Macro Name>}}`
    - context: current rendering context for read and write values
  - __type__: type of macro result (**md**=Markdown expression (default), **macro**=macro syntax)

Result of example:

<{{extend:datetime}}
def = {
  fn: function(param,inside,context) {
    
    return "**"+new Date()+"**";
  },
  type: "md"
}
{{extend}}>
Current date: {{datetime}} !

