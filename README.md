{{template:template1}}

# -Extended markdown
---

Is a markdown extende by macros syntax. Markdown is converted all to macro syntax and render for a specific output.
Implemented output render:
- **html**: Output on html format
- **odt**: Open document format (used by LibreOffice or Open Office).
- **docx**: Microsoft Open Xml document format

# -Index

{{toc}}


{{include:markdown-syntax.md}}

{{include:macros-syntax.md}}



